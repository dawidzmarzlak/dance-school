package pl.zmarzlak.webservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.zmarzlak.webservice.dao.GroupRepository;
import pl.zmarzlak.webservice.dao.NewsRepository;
import pl.zmarzlak.webservice.dao.ScheduleRepository;
import pl.zmarzlak.webservice.dao.UserRepository;
import pl.zmarzlak.webservice.model.Group;
import pl.zmarzlak.webservice.model.Role;
import pl.zmarzlak.webservice.model.User;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = {"http://localhost:3000", "http://192.168.0.52"})
public class GroupController {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @GetMapping("/groups")
    public List<Group> getAllGroups() {
        return groupRepository.findAll();
    }

    @GetMapping("/groups/{id}")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor') or #id == authentication.principal.groupId")
    public Optional<Group> getGroupById(@PathVariable("id") Long id) {
        return groupRepository.findById(id);
    }

    @GetMapping("/instructor/groups/{instructorId}")
    @PreAuthorize("hasAnyAuthority('admin') or #instructorId == authentication.principal.id")
    public List<Group> getGroupByInstructor(@PathVariable("instructorId") Long instructorId) {
        return groupRepository.getGroupsByInstructorId(instructorId);
    }

    @PostMapping("/groups")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor')")
    public Group createGroup(@Valid @RequestBody Group group) {
        if (userRepository.findById(group.getInstructorId()).get().getRole().getName().equals("instructor")) {
            return groupRepository.save(group);
        }
        return null;
    }

    @PostMapping("/groups/{id}")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor')")
    public Group updateGroup(@Valid @RequestBody Group group, @PathVariable Long id) {
        if (groupRepository.findById(id).isPresent()) {
            group.setId(id);
        }
        return groupRepository.save(group);
    }

    @DeleteMapping("/groups/{id}")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor')")
    public void deleteGroup(@PathVariable("id") Long id) {
        Group group = groupRepository.getOne(id);
        newsRepository.deleteAll(newsRepository.getNewsByGroup(group));
        List<User> users = userRepository.getUsersByGroup(group);
        users.forEach((user) -> {
            user.setGroup(null);
            userRepository.save(user);
        });
        groupRepository.deleteById(id);
    }
}
