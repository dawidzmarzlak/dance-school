package pl.zmarzlak.webservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.zmarzlak.webservice.dao.VoteRepository;
import pl.zmarzlak.webservice.model.Vote;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@CrossOrigin(origins = {"http://localhost:3000", "http://192.168.0.52"})
public class VoteController {

    @Autowired
    private VoteRepository voteRepository;


    @PostMapping("/vote")
    @PreAuthorize("#vote.user == authentication.principal.id")
    public void togglePostVote(@RequestBody @Valid Vote vote) {
        Optional<Vote> existingVote = voteRepository.findByBlogPostAndUser(vote.getBlogPost(), vote.getUser());
        if (existingVote.isPresent()) {
            voteRepository.delete(existingVote.get());
        } else {
            voteRepository.save(vote);
        }
    }
}
