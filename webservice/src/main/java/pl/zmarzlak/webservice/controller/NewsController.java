package pl.zmarzlak.webservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.zmarzlak.webservice.dao.GroupRepository;
import pl.zmarzlak.webservice.dao.NewsRepository;
import pl.zmarzlak.webservice.model.Group;
import pl.zmarzlak.webservice.model.News;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@CrossOrigin(origins = {"http://localhost:3000", "http://192.168.0.52"})
public class NewsController {

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private GroupRepository groupRepository;


    @GetMapping("/news")
    public List<News> getAllNews() {
        List<News> news = newsRepository.getNewsByGroup(null);
        Collections.reverse(news);
        return news;
    }

    @GetMapping("/news/{id}")
    public Optional<News> getNewsById(@PathVariable("id") Long id) {
        return newsRepository.findById(id);
    }

    @GetMapping("/news/group/{group}")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor') or #groupId == authentication.principal.groupId")
    public List<News> getNewsForSpecyficGroup(@PathVariable("group") Long groupId) {
        List<News> news = newsRepository.getNewsByGroup(groupRepository.getOne(groupId));
        Collections.reverse(news);
        return news;
    }

    @PostMapping("/news")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor')")
    public News createNews(@RequestBody @Valid News news) {
        long millis = System.currentTimeMillis();
        java.sql.Date date = new java.sql.Date(millis);
        news.setDate(date);
        Group newsGroup = news.getGroup();
        if (Objects.isNull(newsGroup)) {
            news.setGroup(null);
        } else {
            news.setGroup(groupRepository.getOne(newsGroup.getId()));
        }
        return newsRepository.save(news);
    }

    @PostMapping("/news/{id}")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor')")
    public News updateNews(@RequestBody @Valid News news, @PathVariable Long id) {
        long millis = System.currentTimeMillis();
        java.sql.Date date = new java.sql.Date(millis);
        news.setDate(date);
        Group newsGroup = news.getGroup();
        if (Objects.isNull(newsGroup)) {
            news.setGroup(null);
        } else {
            news.setGroup(groupRepository.getOne(newsGroup.getId()));
        }
        if (newsRepository.findById(id).isPresent()) {
            news.setId(id);
        }
        return newsRepository.save(news);
    }

    @DeleteMapping("/news/{id}")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor')")
    public void deleteNewsById(@PathVariable Long id) {
        newsRepository.deleteById(id);
    }
}
