package pl.zmarzlak.webservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import pl.zmarzlak.webservice.dao.BlogRepository;
import pl.zmarzlak.webservice.dao.GroupRepository;
import pl.zmarzlak.webservice.dao.RoleRepository;
import pl.zmarzlak.webservice.dao.UserRepository;
import pl.zmarzlak.webservice.model.Group;
import pl.zmarzlak.webservice.model.Role;
import pl.zmarzlak.webservice.model.User;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@CrossOrigin(origins = {"http://localhost:3000", "http://192.168.0.52"})
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BlogRepository blogRepository;

    @GetMapping("/users")
    @PreAuthorize("hasAnyAuthority('admin')")
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @GetMapping("/users/{id}")
    @PreAuthorize("#id == authentication.principal.id or hasAnyAuthority('admin', 'instructor')")
    public Optional<User> getUser(@PathVariable Long id) {
        return userRepository.findById(id);
    }

    @GetMapping("/students")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor')")
    public List<User> getStudents() {
        return userRepository.getUsersByRole(roleRepository.getRoleByName("student").get());
    }

    @GetMapping("/students/{groupId}")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor')")
    public List<User> getUsersByGroup(@PathVariable Long groupId) {
        return userRepository.getUsersByGroup(groupRepository.getOne(groupId));
    }

    @GetMapping("/instructors")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor')")
    public List<User> getInstructors() {
        return userRepository.getUsersByRole(roleRepository.getRoleByName("instructor").get());
    }

    @PostMapping("/register")
    public User createUser(@RequestBody @Valid User user) {
        Role userRole = roleRepository.getRoleByName(user.getRole().getName()).get();
        user.setRole(userRole);

        if (!Objects.isNull(user.getGroup())) {
            Group userGroup = groupRepository.getOne(user.getGroup().getId());
            user.setGroup(userGroup);
        }

        String password = user.getPassword();
        String encodedPassword = new BCryptPasswordEncoder().encode(password);
        user.setPassword(encodedPassword);

        return userRepository.save(user);
    }

    @PostMapping("/users/{id}")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor') or #id == authentication.principal.id")
    public User updateUser(@RequestBody @Valid User user, @PathVariable Long id) {
        Role userRole = roleRepository.getRoleByName(user.getRole().getName()).get();
        User updateUser = userRepository.findById(id).get();
        updateUser.setRole(userRole);
        updateUser.setEmail(user.getEmail());
        updateUser.setFirstname(user.getFirstname());
        updateUser.setLastname(user.getLastname());
        updateUser.setUsername(user.getUsername());

        if (!user.getPassword().equals("")) {
            String password = user.getPassword();
            String encodedPassword = new BCryptPasswordEncoder().encode(password);
            updateUser.setPassword(encodedPassword);
        }

        if (!Objects.isNull(user.getGroup())) {
            Group userGroup = groupRepository.getOne(user.getGroup().getId());
            updateUser.setGroup(userGroup);
        }

        return userRepository.save(updateUser);
    }

    @DeleteMapping("/users/{id}")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor') or #id == authentication.principal.id")
    public void deleteUserById(@PathVariable Long id) {
        User user = userRepository.findById(id).get();
        blogRepository.deleteBlogsByUser(user);
        if (user.getRole().getName().equals("instructor")) {
            groupRepository.deleteAll(groupRepository.getGroupsByInstructorId(id));
        }
        userRepository.deleteById(id);
    }
}
