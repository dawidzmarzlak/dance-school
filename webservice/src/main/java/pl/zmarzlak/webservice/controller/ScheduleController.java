package pl.zmarzlak.webservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.zmarzlak.webservice.dao.GroupRepository;
import pl.zmarzlak.webservice.dao.ScheduleRepository;
import pl.zmarzlak.webservice.model.Group;
import pl.zmarzlak.webservice.model.Schedule;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = {"http://localhost:3000", "http://192.168.0.52"})
public class ScheduleController {

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private GroupRepository groupRepository;

    @GetMapping("/schedules")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor')")
    public List<Schedule> getSchedules() {
        return scheduleRepository.findAll();
    }

    @GetMapping("/schedules/{groupId}")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor') or #groupId == authentication.principal.groupId")
    public List<Schedule> getSchedulesByGroup(@PathVariable Long groupId) {
        return scheduleRepository.getSchedulesByGroup(groupRepository.getOne(groupId));
    }

    @GetMapping("/instructor/schedules/{instructorId}")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor') or #instructorId == authentication.principal.id")
    public List<Schedule> getSchedulesByInstructor(@PathVariable Long instructorId) {
        return scheduleRepository.getSchedulesByGroupInstructorId(instructorId);
    }

    @PostMapping("/schedules")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor')")
    public Schedule createSchedule(@RequestBody @Valid Schedule schedule) {
        Group group = groupRepository.getOne(schedule.getGroup().getId());
        schedule.setGroup(group);
        return scheduleRepository.save(schedule);
    }

    @PostMapping("/schedules/{id}")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor')")
    public Schedule updateSchedule(@RequestBody @Valid Schedule schedule, @PathVariable Long id) {
        Group group = groupRepository.getOne(schedule.getGroup().getId());
        schedule.setGroup(group);
        if (scheduleRepository.findById(id).isPresent()) {
            schedule.setId(id);
        }
        return scheduleRepository.save(schedule);
    }

    @DeleteMapping("/schedules/{id}")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor')")
    public void deleteScheduleById(@PathVariable Long id) {
        scheduleRepository.deleteById(id);
    }
}
