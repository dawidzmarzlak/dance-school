package pl.zmarzlak.webservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.zmarzlak.webservice.dao.BlogRepository;
import pl.zmarzlak.webservice.dao.GroupRepository;
import pl.zmarzlak.webservice.dao.UserRepository;
import pl.zmarzlak.webservice.dao.VoteRepository;
import pl.zmarzlak.webservice.model.Blog;
import pl.zmarzlak.webservice.model.Group;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@CrossOrigin(origins = {"http://localhost:3000", "http://192.168.0.52"})
public class BlogController {

    @Autowired
    private BlogRepository blogRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private VoteRepository voteRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/blog/{id}")
    @PostAuthorize("hasAnyAuthority('admin', 'instructor') or returnObject.get().user.id == authentication.principal.id")
    public Optional<Blog> getBlogPostById(@PathVariable("id") Long id) {
        return blogRepository.findById(id);
    }

    @GetMapping("/blog/group/{group}")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor') or #groupId == authentication.principal.groupId")
    public List<Blog> getBlogForSpecificGroup(@PathVariable("group") Long groupId) {
        List<Blog> blogs = blogRepository.getBlogsByGroup(groupRepository.getOne(groupId));
        Collections.reverse(blogs);
        return blogs;
    }

    @PostMapping("/blog/group/{group}")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor') or #groupId == authentication.principal.groupId")
    public Blog createBlog(@RequestBody @Valid Blog blog, @PathVariable("group") Long groupId) {
        long millis = System.currentTimeMillis();
        java.sql.Date date = new java.sql.Date(millis);
        blog.setDate(date);
        blog.setGroup(groupRepository.getOne(groupId));
        blog.setUser(userRepository.getOne(blog.getUser().getId()));
        return blogRepository.save(blog);
    }

    @PostMapping("/blog/{id}")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor') or #blog.user.id == authentication.principal.id")
    public Blog updateBlog(@RequestBody @Valid Blog blog, @PathVariable Long id) {
        long millis = System.currentTimeMillis();
        java.sql.Date date = new java.sql.Date(millis);
        blog.setDate(date);
        Group blogGroup = blog.getGroup();
        blog.setUser(userRepository.getOne(blog.getUser().getId()));
        if (!Objects.isNull(blogGroup)) {
            blog.setGroup(groupRepository.getOne(blogGroup.getId()));
        }
        if (blogRepository.findById(id).isPresent()) {
            blog.setId(id);
        }
        return blogRepository.save(blog);
    }

    @DeleteMapping("/blog/{id}")
    @PreAuthorize("hasAnyAuthority('admin', 'instructor')or #blog.user.id == authentication.principal.id")
    public void deleteBlogById(@PathVariable Long id, @RequestBody @Valid Blog blog) {
        voteRepository.deleteAllByBlogPost(id);
        blogRepository.deleteById(id);
    }
}
