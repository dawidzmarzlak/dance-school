package pl.zmarzlak.webservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "`vote`")
@EntityListeners(AuditingEntityListener.class)
public class Vote {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "user")
    private Long user;

    @Column(name = "`blog_post`")
    private Long blogPost;

    public Vote() {
    }

    public Vote(Long user, Long blogPost) {
        this.user = user;
        this.blogPost = blogPost;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getUser() {
        return user;
    }

    public void setUser(Long user) {
        this.user = user;
    }

    public Long getBlogPost() {
        return blogPost;
    }

    public void setBlogPost(Long blogPost) {
        this.blogPost = blogPost;
    }
}
