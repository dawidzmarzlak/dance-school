package pl.zmarzlak.webservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zmarzlak.webservice.model.Blog;
import pl.zmarzlak.webservice.model.Group;
import pl.zmarzlak.webservice.model.User;

import java.util.List;

@Repository
public interface BlogRepository extends JpaRepository<Blog, Long> {

    List<Blog> getBlogsByGroup(Group group);
    void deleteBlogsByUser(User user);
}
