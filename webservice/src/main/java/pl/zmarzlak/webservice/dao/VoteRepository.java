package pl.zmarzlak.webservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zmarzlak.webservice.model.Vote;

import java.util.Optional;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Long> {

    void deleteAllByBlogPost(Long blog);
    Optional<Vote> findByBlogPostAndUser(Long blog, Long user);
}
