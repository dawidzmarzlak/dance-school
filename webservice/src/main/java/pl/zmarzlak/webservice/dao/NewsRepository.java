package pl.zmarzlak.webservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zmarzlak.webservice.model.Group;
import pl.zmarzlak.webservice.model.News;

import java.util.List;

@Repository
public interface NewsRepository extends JpaRepository<News, Long> {

    List<News> getNewsByGroup(Group group);
}
