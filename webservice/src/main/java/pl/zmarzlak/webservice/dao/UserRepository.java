package pl.zmarzlak.webservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zmarzlak.webservice.model.Group;
import pl.zmarzlak.webservice.model.Role;
import pl.zmarzlak.webservice.model.User;

import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    List<User> getUsersByGroup(Group group);
    List<User> getUsersByRole(Role role);
    Optional<User> getUserByUsername(String username);
}
