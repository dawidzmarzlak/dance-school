export const userRoles = {
    admin: 'Administrator',
    student: 'Uczeń',
    instructor: 'Instruktor'
};