import axios from 'axios';

export const API_ADDRESS = 'http://localhost:8080';

export const getRequest = (url, path, handleError, handleResponse) => {
    const user = JSON.parse(localStorage.getItem('user'));
    const token = user ? user.token : '';
    path = path ? `/${path}` : ''; 
    axios.get(`${API_ADDRESS}${url}${path}`, {
            headers: {'Authorization': `Bearer ${token}`}
        }).then((response) => {
            handleResponse(response);
        }, (error) => {
            handleError(error);
        });
};

export const postRequest = (url, data, handleError, handleResponse) => {
    const user = JSON.parse(localStorage.getItem('user'));
    const token = user ? user.token : '';
    axios.post(`${API_ADDRESS}${url}`, data, {
            headers: {'Authorization': `Bearer ${token}`}
        }).then((response) => {
            handleResponse(response);
        }, (error) => {
            handleError(error);
        });
};

export const deleteRequest = (url, path, handleError, handleResponse) => {
    const user = JSON.parse(localStorage.getItem('user'));
    const token = user ? user.token : '';
    axios.delete(`${API_ADDRESS}${url}/${path}`, {
            headers: {'Authorization': `Bearer ${token}`}
        }).then((response) => {
            handleResponse(response);
        }, (error) => {
            handleError(error);
        });
};
