import { useEffect, useState } from 'react';
import { Button, Form, Image } from 'react-bootstrap';
import { API_ADDRESS, getRequest, postRequest } from '../../lib/communication';
import axios from 'axios';
import _ from 'lodash';
import { useParams, useHistory } from 'react-router-dom';

const EditBlogPost = ({handleError}) => {
    const { postId, groupId } = useParams();
    const history = useHistory();
    const [image, setImage] = useState('');
    const [content, setContent] = useState('');

    useEffect(() => {
        if (postId) {
            getRequest('/blog', postId, handleError, (response) => {
                setContent(response.data.content);
                setImage(response.data.image);
            });
        }
    }, [postId]);

    const handleSubmit = (event) => {
        event.preventDefault();
        const group = {id: groupId};
        const user = JSON.parse(localStorage.getItem('user'));
        const data = {
            image: image,
            content: content,
            group: group,
            user: {id: user.id},
        };

        if (postId) {
            postRequest(`/blog/${postId}`, data, (error) => {
                handleError(error);
                console.log(error);
            }, (response) => {
                history.goBack();
            });
        } else {
            postRequest(`/blog/group/${groupId}`, data, (error) => {
                handleError(error);
                console.log(error);
            }, (response) => {
                history.goBack();
            });
        }
    };

    const handleDeletePost = () => {
        const confirm = window.confirm('Czy na pewno chcesz usunąć post?');
        if (confirm) {
            const user = JSON.parse(localStorage.getItem('user'));
            const group = {id: groupId};
            const token = user ? user.token : '';
            const data = {
                image: image,
                content: content,
                group: group,
                user: {id: user.id},
            };

            axios.delete(`${API_ADDRESS}/blog/${postId}`, {
                headers: {'Authorization': `Bearer ${token}`},
                data: data
            }).then((response) => {
                alert("Pomyślnie usunięto post");
                history.goBack(); 
            }, (error) => {
                handleError(error);
            });
        }
    };

    const handleChooseImage = (file) => {
        const reader = new FileReader();
        console.log('dsd')
        reader.onload = (upload) => {
            console.log(upload.target.result);
            setImage(upload.target.result.split(',')[1]);
        };

        reader.readAsDataURL(file);
    };

    const fileDialog = () => new Promise(resolve => {
        const input = document.createElement('input');
        input.type = 'file';
        input.accept = 'image/*';
        input.onchange = (event) => {
            resolve(event.target.files);
        };
        input.click();
    });

    const chooseImage = () => {
        fileDialog().then((files) => handleChooseImage(files[0]));
    };

    return (<>
        {postId && <h1 className="App-page-title">Edytuj post</h1>}
        {!postId && <h1 className="App-page-title">Dodaj nowy post</h1>}
        <div  className="App-content">
            <div style={{width: '100%'}}>  
                <Form onSubmit={handleSubmit} style={{margin: '0 auto', width: '100%'}} className="App-register-form">
                    <Form.Group controlId="formContent">
                        <Image className="App-blog-image" src={`data:image/jpeg;base64,${image}`} alt="" />
                    </Form.Group>
                    <Form.Group controlId="formContent">
                        <Form.Label>Opis</Form.Label>
                        <Form.Control as="textarea" rows={3} value={content} onChange={(event) => setContent(event.target.value)} />
                    </Form.Group>
                    <Form.Group controlId="formChooseImageButton">
                        <Button variant="primary" onClick={chooseImage}>Wybierz zdjęcie</Button>
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        {postId ? 'Zapisz' : 'Dodaj'}
                    </Button>
                </Form>
                {postId && <Button variant="danger" style={{margin: 20}} onClick={handleDeletePost}>Usuń</Button>}             
            </div>
        </div>
    </>);
}

export default EditBlogPost;