import { getRequest, postRequest } from '../../lib/communication';
import { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import _ from 'lodash';
import { FaEdit, FaHeart, FaRegHeart } from 'react-icons/fa';
import { Button, Image } from 'react-bootstrap';

const BlogPosts = ({handleError}) => {
    const { groupId } = useParams();
    const [posts, setPosts] = useState(null);
    const [groupInfo, setGroupInfo] = useState(null);
    const [userDetails, setUserDetails] = useState('');
    const history = useHistory();

    useEffect(() => {
        getRequest('/blog/group', groupId, handleError, (response) => {
            setPosts(response.data);
        });
        
        getRequest('/groups', groupId, handleError, (response) => {
            setGroupInfo(response.data);
        });
    }, [groupId]);

    useEffect(() => {
        const user = JSON.parse(localStorage.getItem('user'));
        
        if (user) {
            getRequest('/users', user.id, handleError, (response) => {
                setUserDetails(response.data);
            });
        }
    }, []);

    const isAdminOrInstructor = () => {
        if (!userDetails) {
            return false;
        } else {
            return ['admin', 'instructor'].includes(userDetails.role.name);
        }
    };

    const handleToggleVote = (postId) => {
        const vote = { user: userDetails.id, blogPost: postId };
        postRequest('/vote', vote, handleError, (response) => {
            const newPosts = _.cloneDeep(posts);
            const postToEditId = _.findIndex(posts, {id: postId});
            if (newPosts[postToEditId].votes) {
                const toDelete = _.findIndex(newPosts[postToEditId].votes, (elem) => elem.user === vote.user);
                if (toDelete !== -1) {
                    newPosts[postToEditId].votes = [..._.filter(newPosts[postToEditId].votes, (elem) => elem.user !== vote.user)];
                } else {
                    newPosts[postToEditId].votes = [...newPosts[postToEditId].votes, vote]; 
                }
            } else {
                newPosts[postToEditId].votes = [...newPosts[postToEditId].votes, vote];
            }
            setPosts(newPosts);
        });
    };

    return (<>
        <h1 className="App-page-title">Tablica</h1>
        {groupInfo && <h3>Grupa {groupInfo.name}</h3>}
        <Button onClick={() => {
            history.push(`/blog/group/${groupId}/add`); 
        }}>
            Dodaj post
        </Button>
        <div className="App-content">
            {posts && <div className="App-news-list">
                {_.map(posts, (elem) => {
                    return (<div className="App-news" key={elem.id}>
                        <div className="App-news-title">
                            {`${elem.user.firstname} ${elem.user.lastname} `} 
                            {(isAdminOrInstructor() || elem.user.id === userDetails.id) 
                            && <span className="App-edit-news-button" onClick={() => {
                                history.push(`/blog/group/${groupId}/edit/${elem.id}`);
                            }}><FaEdit /></span>}
                        </div>
                        <div className="App-news-date">{elem.date}</div>
                        <Image className="App-blog-image" src={`data:image/jpeg;base64,${elem.image}`} alt="" />
                        <div className="App-blog-content">{elem.content}</div>
                        <div className="App-blog-votes">{`Ilość polubień: ${elem.votes.length || 0}`}</div>
                        <span onClick={() => handleToggleVote(elem.id)} className="App-edit-news-button">
                            {elem.votes ? (_.find(elem.votes, {user: userDetails.id}) ? <FaHeart size={24} /> : <FaRegHeart size={24} />) : <FaRegHeart size={24} />}
                        </span>
                    </div>);
                })}
            </div>}
        </div>
    </>);
}

export default BlogPosts;