import { useEffect, useState } from 'react';
import { Button, Alert, Form } from 'react-bootstrap';
import { deleteRequest, getRequest, postRequest } from '../../lib/communication';
import _ from 'lodash';
import { useParams, useHistory, Link } from 'react-router-dom';

const EditGroup = ({handleError, defMode = ''}) => {
    const [groupDetails, setGroupDetails] = useState(null);
    const { groupId } = useParams();
    const history = useHistory();
    const [mode, setMode] = useState(defMode);
    const [status, setStatus] = useState(null);
    const [instructors, setInstructors] = useState([]);
    const [name, setName] = useState('');
    const [instructorId, setInstructorId] = useState('');
    const [instructor, setInstructor] = useState(null);

    useEffect(() => {
        getRequest('/instructors', '', handleError, (response) => {
            setInstructors(response.data);
        });
    }, []);

    const handleSubmit = (event) => {
        event.preventDefault();

        const data = {
            name: name,
            instructorId: Number(instructorId),
        };

        if (groupId) {
            postRequest(`/groups/${groupId}`, data, (error) => {
                handleError(error);
                setStatus('error');
                console.log(error);
            }, (response) => {
                setGroupDetails(response.data);
                setStatus('successEdit');
                setMode('');
            });
        } else {
            postRequest(`/groups`, data, (error) => {
                handleError(error);
                setStatus('error');
                console.log(error);
            }, (response) => {
                setGroupDetails(response.data);
                setStatus('successAdd');
                setMode('');
            });
        }
        

        window.scrollTo(0, 0);
    };

    const handleDeleteGroup = () => {
        const confirm = window.confirm('Czy na pewno chcesz usunąć grupę?');
        if (confirm) {
            deleteRequest('/groups', groupDetails.id, handleError, (response) => {
                alert("Pomyślnie usunięto grupę");
                history.goBack(); 
            });
        }
    };

    useEffect(() => {
        if (groupId) {
            getRequest('/groups', groupId, handleError, (response) => {
                setGroupDetails(response.data);
            });
        }
    }, []);

    useEffect(() => {
        if (groupDetails) {
            setName(groupDetails.name);
            setInstructorId(groupDetails.instructorId);
        } else if (instructors.length) {
            setInstructorId(instructors[0].id);
        }
    }, [groupDetails, instructors]);

    useEffect(() => {
        if (groupDetails) {
            setInstructor(_.find(instructors, (instructorElem) => instructorElem.id === groupDetails.instructorId));
        }
    }, [instructors, groupDetails, mode]);

    return (<>
        {instructors.length && <>
            {groupDetails && <h1 className="App-page-title">Grupa {groupDetails.name}</h1>}
            {!groupDetails && mode === 'edit' && <h1 className="App-page-title">Dodaj nową grupę</h1>}
            <div  className="App-content">
                <div style={{width: '100%'}}>
                    {mode === '' && groupDetails && instructor && <>
                        {status === 'successEdit' && <Alert variant="success" className="App-register-alert">Pomyślnie edytowano grupę</Alert>}
                        {status === 'successAdd' && <Alert variant="success" className="App-register-alert">Pomyślnie dodano grupę</Alert>}
                        <div>
                            <div>
                                Nazwa: <b>{groupDetails.name}</b><br />
                                Instruktor: <b>{instructor.firstname} {instructor.lastname}</b><br />
                            </div>
                        </div>
                        <Button className="App-edit-user-button" onClick={() => setMode('edit')}>Edytuj</Button>
                        <Button className="App-edit-user-button" style={{width: 'fit-content'}} as={Link} to={`/news/group/${groupDetails.id}`} >Wyświetl aktualności grupy</Button>
                        <Button className="App-edit-user-button" style={{width: 'fit-content'}} as={Link} to={`/students/${groupDetails.id}`} >Wyświetl uczniów grupy</Button>
                        <Button className="App-edit-user-button" style={{width: 'fit-content'}} as={Link} to={`/schedule/${groupDetails.id}`} >Wyświetl plan grupy</Button>
                        <Button className="App-edit-user-button" style={{width: 'fit-content'}} as={Link} to={`/blog/group/${groupDetails.id}`} >Wyświetl tablicę grupy</Button>
                        <Button className="App-edit-user-button" variant="danger" onClick={handleDeleteGroup}>Usuń grupę</Button>
                    </>}
                    {mode === 'edit' && <>
                        {status === 'error' && <Alert variant="warning" className="App-register-alert">Wystąpił problem, spróbuj ponownie później.</Alert>}
                        <Form onSubmit={handleSubmit} style={{margin: '0 auto'}} className="App-register-form">
                            <Form.Group controlId="formFirstname">
                                <Form.Label>Nazwa</Form.Label>
                                <Form.Control type="text" value={name} onChange={event => setName(event.target.value)} placeholder="Nazwa" required />
                            </Form.Group>
                            <Form.Group controlId="formGroupSelect">
                                <Form.Label>Instruktor</Form.Label>
                                {instructors.length && <Form.Control as="select" value={instructorId} onChange={(event) => setInstructorId(event.target.value)}>
                                    {_.map(instructors, (instructorElem) => (<option key={`instructor_${instructorElem.id}`} value={instructorElem.id}>{instructorElem.firstname} {instructorElem.lastname}</option>))}
                                </Form.Control>}
                            </Form.Group>    
                            <Button variant="primary" type="submit">
                                Zapisz
                            </Button>
                        </Form>
                    </>}
                </div>
            </div>
        </>}
    </>);
}

export default EditGroup;