import { FaMapMarkerAlt, FaMap, FaPhoneAlt, FaEnvelope } from 'react-icons/fa';

const Contact = () => {
    return (
        <>
            <h1 className="App-page-title">Skontaktuj się z nami</h1>
            <div className="App-content">
                <div><img src="/salsa.png" height={372} alt=""/></div>
                <div>
                    <h3>Szkoła tańca</h3>
                    <FaMapMarkerAlt />{' '}Warszawska 1<br />
                    <FaMap />{' '}01-000 Warszawa<br />
                    <FaPhoneAlt />{' '}123 456 789<br />
                    <FaEnvelope />{' '}kontakt@szkolatanca.pl<br />
                </div>
            </div>
        </>
    );
}

export default Contact;