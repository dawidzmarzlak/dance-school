import { useEffect, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { deleteRequest, getRequest, postRequest } from '../../lib/communication';
import _ from 'lodash';
import { useParams, useHistory } from 'react-router-dom';

const EditNews = ({handleError}) => {
    const { newsId, groupId } = useParams();
    const history = useHistory();
    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');

    useEffect(() => {
        if (newsId) {
            getRequest('/news', newsId, handleError, (response) => {
                setContent(response.data.content);
                setTitle(response.data.shortDescription)
            });
        }
    }, []);

    const handleSubmit = (event) => {
        event.preventDefault();
        const group = groupId ? {id: groupId} : null;
        const data = {
            shortDescription: title,
            content: content,
            group: group,
        };

        if (newsId) {
            postRequest(`/news/${newsId}`, data, (error) => {
                handleError(error);
                console.log(error);
            }, (response) => {
                history.goBack();
            });
        } else {
            postRequest(`/news`, data, (error) => {
                handleError(error);
                console.log(error);
            }, (response) => {
                history.goBack();
            });
        }
    };

    const handleDeleteNews = () => {
        const confirm = window.confirm('Czy na pewno chcesz usunąć news?');
        if (confirm) {
            deleteRequest('/news', newsId, handleError, (response) => {
                alert("Pomyślnie usunięto news");
                history.goBack(); 
            });
        }
    };

    return (<>
        {newsId && <h1 className="App-page-title">Edytuj news</h1>}
        {!newsId && <h1 className="App-page-title">Dodaj nowy news</h1>}
        <div  className="App-content">
            <div style={{width: '100%'}}>  
                <Form onSubmit={handleSubmit} style={{margin: '0 auto', width: '100%'}} className="App-register-form">
                    <Form.Group controlId="formTitle">
                        <Form.Label>Tytuł</Form.Label>
                        <Form.Control type="text" value={title} onChange={event => setTitle(event.target.value)} placeholder="Tytuł" required />                            </Form.Group>
                    <Form.Group controlId="formContent">
                        <Form.Label>Treść</Form.Label>
                        <Form.Control as="textarea" rows={20} value={content} onChange={(event) => setContent(event.target.value)} required />
                    </Form.Group>    
                    <Button variant="primary" type="submit">
                        {newsId ? 'Zapisz' : 'Dodaj'}
                    </Button>
                </Form>
                {newsId && <Button variant="danger" style={{margin: 20}} onClick={handleDeleteNews}>Usuń</Button>}             
            </div>
        </div>
    </>);
}

export default EditNews;