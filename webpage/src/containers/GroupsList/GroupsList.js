import { useEffect, useState } from 'react';
import { Button, Row } from 'react-bootstrap';
import { getRequest } from '../../lib/communication';
import _ from 'lodash';
import { Link, useHistory, useParams } from 'react-router-dom';

const GroupsList = ({handleError, getUrl, title}) => {
    const [groupsList, setGroupsList] = useState([]);
    const history = useHistory();
    const { instructorId } = useParams();

    useEffect(() => {
        const getPathParams = instructorId || '';
        getRequest(getUrl, getPathParams, handleError, (response) => {
            setGroupsList(response.data);
        });
    }, []);

    return (<>
        <h1 className="App-page-title">{title}</h1>
        <div  className="App-content">
            <Button style={{marginBottom: 20}} as={Link} to="/groups/add">Dodaj grupę</Button>
            <div className="App-user-list">
                {_.map(groupsList, (group) => 
                    <Row key={group.id} className="App-user-row" onClick={() => history.push(`/groups/edit/${group.id}`)}>
                        <div style={{width: '100%', textAlign: 'left'}}>
                            <div>{group.name}</div>
                        </div>
                    </Row>
                )}
            </div>
        </div>
    </>);
}

export default GroupsList;