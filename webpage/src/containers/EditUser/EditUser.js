import { useEffect, useState } from 'react';
import { Button, Alert, Form } from 'react-bootstrap';
import { deleteRequest, getRequest, postRequest } from '../../lib/communication';
import _ from 'lodash';
import { useParams, useHistory } from 'react-router-dom';
import { userRoles } from '../../lib/user';

const EditUser = ({handleError, handleLogout}) => {
    const [userDetails, setUserDetails] = useState(null);
    const { userId } = useParams();
    const history = useHistory();
    const [mode, setMode] = useState('');
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [passwordRepeat, setPasswordRepeat] = useState('');
    const [firstname, setFirstname] = useState('');
    const [lastname, setLastname] = useState('');
    const [email, setEmail] = useState('');
    const [groupId, setGroupId] = useState('');
    const [status, setStatus] = useState(null);
    const [role, setRole] = useState('');
    const roles = ['admin', 'instructor', 'student'];
    const [groups, setGroups] = useState([]);
    const [editingUser, setEditingUser] = useState(null);

    useEffect(() => {
        getRequest('/groups', '', (error) => {console.error(error);}, (response) => {
            setGroups(response.data);
        });
    }, []);

    useEffect(() => {
        const id = JSON.parse(localStorage.getItem('user')).id;
        getRequest('/users', id, handleError, (response) => {
            setEditingUser(response.data);
        });
    }, []);

    const handleSubmit = (event) => {
        event.preventDefault();

        const data = {
            username: login,
            password: mode === 'changePassword' ? password : '',
            firstname: firstname,
            lastname: lastname,
            email: email,
            group: {id: groupId || groups[0].id},
            role: {name: role},
        };

        if (mode === 'changePassword') {
            if (password !== passwordRepeat) {
                setStatus('different passwords');
                setPassword('');
                setPasswordRepeat('');
                window.scrollTo(0, 0);
                return;
            }
            
        }

        postRequest(`/users/${userId}`, data, (error) => {
            handleError(error);
            if (error.response.status === 500) {
                setStatus('bad data');
            } else {
                setStatus('error');
            }
            console.log(error);
        }, (response) => {
            setUserDetails(response.data);
            setStatus(mode === 'changePassword' ? 'successChangePassword' : 'successEdit');
            setMode('');
        });

        window.scrollTo(0, 0);
    };

    const handleDeleteAccount = () => {
        const confirm = window.confirm('Czy na pewno chcesz usunąć konto?');
        if (confirm) {
            deleteRequest('/users', userDetails.id, handleError, (response) => {
                const loggedUserId = JSON.parse(localStorage.getItem('user')).id;
                alert("Pomyślnie usunięto konto");
                if (loggedUserId === userDetails.id) {
                    handleLogout();
                } else {
                    history.goBack();
                }   
            });
        }
    };

    useEffect(() => {
        getRequest('/users', userId, handleError, (response) => {
            setUserDetails(response.data);
        });
    }, []);



    useEffect(() => {
        if (userDetails) {
            setEmail(userDetails.email);
            setLogin(userDetails.username);
            setFirstname(userDetails.firstname);
            setLastname(userDetails.lastname);
            setRole(userDetails.role.name);
            setGroupId(userDetails.group ? userDetails.group.id : '');
        }
    }, [userDetails]);

    return (<>
        {userDetails && <>
            <h1 className="App-page-title">Użytkownik {userDetails.username}</h1>
            <div  className="App-content">
                <div style={{width: '100%'}}>
                    {mode === '' && <>
                        {status === 'successChangePassword' && <Alert variant="success" className="App-register-alert">Pomyślnie zmieniono hasło</Alert>}
                        {status === 'successEdit' && <Alert variant="success" className="App-register-alert">Pomyślnie edytowano dane konta</Alert>}
                        <div>
                            <div>
                                Imię i nazwisko: <b>{`${userDetails.firstname} ${userDetails.lastname}`}</b><br />
                                Email: <b>{userDetails.email}</b><br />
                                Grupa: <b>{userDetails.group ? userDetails.group.name : ''}</b><br />
                            </div>
                        </div>
                        <Button className="App-edit-user-button" onClick={() => setMode('edit')}>Edytuj</Button>
                        <Button className="App-edit-user-button" onClick={() => setMode('changePassword')}>Zmień hasło</Button>
                        <Button className="App-edit-user-button" variant="danger" onClick={handleDeleteAccount}>Usuń konto</Button>
                    </>}
                    {mode === 'edit' && editingUser && <>
                        {status === 'bad data' && <Alert variant="danger" className="App-register-alert">Wprowadzono błędne dane, podany login lub email został już użyty.</Alert>}
                        {status === 'error' && <Alert variant="warning" className="App-register-alert">Wystąpił problem, spróbuj ponownie później.</Alert>}
                        <Form onSubmit={handleSubmit} style={{margin: '0 auto'}} className="App-register-form">
                            <Form.Group controlId="formFirstname">
                                <Form.Label>Imię</Form.Label>
                                <Form.Control type="text" value={firstname} onChange={event => setFirstname(event.target.value)} placeholder="Imię" required />
                            </Form.Group>
                            <Form.Group controlId="formLastname">
                                <Form.Label>Nazwisko</Form.Label>
                                <Form.Control type="text" value={lastname} onChange={event => setLastname(event.target.value)} placeholder="Nazwisko" required />
                            </Form.Group>
                            <Form.Group controlId="formEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" value={email} onChange={event => setEmail(event.target.value)} placeholder="Email" required />
                            </Form.Group>
                            <Form.Group controlId="formLogin">
                                <Form.Label>Login</Form.Label>
                                <Form.Control type="text" value={login} onChange={event => setLogin(event.target.value)} placeholder="Login" required />
                            </Form.Group>
                            <Form.Group controlId="formGroupSelect">
                                <Form.Label>Grupa</Form.Label>
                                {groups.length && <Form.Control as="select" value={groupId} onChange={(event) => setGroupId(event.target.value)}>
                                    {_.map(groups, (group) => (<option key={`group_${group.id}`} value={group.id}>{group.name}</option>))}
                                </Form.Control>}
                            </Form.Group>
                            {editingUser.role.name === 'admin' && <Form.Group controlId="formGroupSelect">
                                <Form.Label>Rola</Form.Label>
                                {roles.length && <Form.Control as="select" value={role} onChange={(event) => setRole(event.target.value)}>
                                    {_.map(roles, (roleElem) => (<option key={`role_${roleElem}`} value={roleElem}>{userRoles[roleElem]}</option>))}
                                </Form.Control>}
                            </Form.Group>}
                            <Button variant="primary" type="submit">
                                Zapisz
                            </Button>
                        </Form>
                    </>}
                    {mode === 'changePassword' && <>
                        {status === 'different passwords' && <Alert variant="danger" className="App-register-alert">Podane hasła są różne.</Alert>}
                        {status === 'error' && <Alert variant="warning" className="App-register-alert">Wystąpił problem, spróbuj ponownie później.</Alert>}
                        <Form onSubmit={handleSubmit} style={{margin: '0 auto'}} className="App-register-form">
                        <Form.Group controlId="formPassword">
                            <Form.Label>Hasło</Form.Label>
                                <Form.Control type="password" value={password} onChange={event => setPassword(event.target.value)} placeholder="Hasło" required />
                            </Form.Group>
                            <Form.Group controlId="formPasswordRepeat">
                                <Form.Label>Powtórz hasło</Form.Label>
                                <Form.Control type="password" value={passwordRepeat} onChange={event => setPasswordRepeat(event.target.value)} placeholder="Powtórz hasło" required />
                            </Form.Group>
                            <Button variant="primary" type="submit">
                                Zmień hasło
                            </Button>
                        </Form>
                    </>}
                </div>
            </div>
        </>}
    </>);
}

export default EditUser;