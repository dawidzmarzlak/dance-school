import Footer from '../../components/Footer';
import Header from '../../components/Header';
import { BrowserRouter as Router, Switch, Route, useLocation, useHistory } from "react-router-dom";
import './App.scss';
import Contact from '../Contact/Contact';
import News from '../News/News';
import Profile from '../Profile/Profile';
import Login from '../Login/Login';
import Home from '../Home/Home';
import Register from '../Register/Register';
import Schedule from '../Schedule/Schedule';
import { Container } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import UserList from '../UserList/UserList';
import EditUser from '../EditUser/EditUser';
import GroupsList from '../GroupsList/GroupsList';
import EditGroup from '../EditGroup/EditGroup';
import EditNews from '../EditNews/EditNews';
import BlogPosts from '../BlogPosts/BlogPosts';
import EditBlogPost from '../EditBlogPost/EditBlogPost';

const App = () => {
  const [logged, setLogged] = useState(false);
  // const location = useLocation();
  // const history = useHistory();

  useEffect(() => {
    const user = localStorage.getItem('user');
    if (user) {
      setLogged(true);
    }
  }, []);

  const handleLogout = () => {
    setLogged(false);
    localStorage.removeItem('user');
  };

  
  const handleError = (error) => {
    console.error(error);
    if (error.response.status === 401) {
        handleLogout();
    }
  };

  return (
    <div className="App">
      <Router>
        <Header logged={logged} />
        <Container>
          <Switch>
            <Route path="/contact">
              <Contact />
            </Route>
            <Route path="/news/group/:groupId/edit/:newsId">
              {logged ? <EditNews handleError={handleError} /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/news/group/:groupId/add">
              {logged ? <EditNews handleError={handleError} /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/news/group/:groupId">
              {logged ? <News handleError={handleError} /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/news/edit/:newsId">
              {logged ? <EditNews handleError={handleError} /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/news/add">
              {logged ? <EditNews handleError={handleError} /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/news">
              <News handleError={handleError} />
            </Route>
            <Route path="/blog/group/:groupId/edit/:postId">
              {logged ? <EditBlogPost handleError={handleError} /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/blog/group/:groupId/add">
              {logged ? <EditBlogPost handleError={handleError} /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/blog/group/:groupId">
              {logged ? <BlogPosts handleError={handleError} /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/profile">
              {logged ? <Profile handleLogout={handleLogout} handleError={handleError} /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/groups/add">
              {logged ? <EditGroup handleError={handleError} defMode="edit" /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/groups/edit/:groupId">
              {logged ? <EditGroup handleError={handleError} /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/groups/instructor/:instructorId">
              {logged ? <GroupsList handleError={handleError} getUrl="/instructor/groups" title="Grupy instruktora" /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/groups">
              {logged ? <GroupsList handleError={handleError} getUrl="/groups" title="Wszystkie grupy" /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/users/edit/:userId">
              {logged ? <EditUser handleError={handleError} handleLogout={handleLogout} /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/students/:groupId">
              {logged ? <UserList handleError={handleError} getUrl="/students" title="Użytkownicy w grupie" /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/users">
              {logged ? <UserList handleError={handleError} getUrl="/users" title="Wszyscy użytkownicy" /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/students">
              {logged ? <UserList handleError={handleError} getUrl="/students" title="Wszyscy uczniowie" /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/instructors">
              {logged ? <UserList handleError={handleError} getUrl="/instructors" title="Wszyscy instruktorzy" /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/register">
              {!logged ? <Register /> : <Profile handleLogout={handleLogout} handleError={handleError} />}
            </Route>
            <Route path="/schedule/instructor/:instructorId">
              {logged ? <Schedule handleError={handleError} view="instructor" /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/schedule/:groupId">
              {logged ? <Schedule handleError={handleError} view="group" /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/schedule">
              {logged ? <Schedule handleError={handleError} view="admin" /> : <Login handleLogin={() => setLogged(true)} />}
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Container>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
