import { Form, Button } from 'react-bootstrap';
import { useState } from 'react';
import axios from 'axios';
import { API_ADDRESS } from '../../lib/communication';

const Login = ({handleLogin}) => {
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [loginStatus, setLoginStatus] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();
        setLoginStatus('Logowanie...');
        axios.post(`${API_ADDRESS}/authenticate`, {
            username: login, password: password
        }).then((response) => {
            localStorage.setItem('user', JSON.stringify(response.data));
            setLoginStatus('');
            handleLogin();
        }, (error) => {
            setLoginStatus('Błędne logowanie');
            console.log(error);
        });
    };

    return (<>
        <h1 className="App-page-title">Zaloguj się</h1>
        <div className="App-content">
            <Form onSubmit={handleSubmit}>
                <Form.Group controlId="formAuthLogin">
                    <Form.Label>Login</Form.Label>
                    <Form.Control type="text" value={login} onChange={event => setLogin(event.target.value)} placeholder="Login" required />
                </Form.Group>
                <Form.Group controlId="formAuthPassword">
                    <Form.Label>Hasło</Form.Label>
                    <Form.Control type="password" value={password} onChange={event => setPassword(event.target.value)} placeholder="Hasło" required />
                </Form.Group>
                <Form.Group>
                    <Form.Label>{loginStatus}</Form.Label>
                </Form.Group>
                <Button variant="primary" type="submit">
                    Zaloguj się
                </Button>
            </Form>
        </div>
    </>);
}

export default Login;