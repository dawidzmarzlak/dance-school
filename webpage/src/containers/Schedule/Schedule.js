import { deleteRequest, getRequest, postRequest } from '../../lib/communication';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import moment from 'moment';
import WeekCalendar from 'react-week-calendar';
import 'react-week-calendar/dist/style.css';
import _ from 'lodash';
import { Button } from 'react-bootstrap';
import IntervalEvent from '../../components/IntervalEvent';
import ScheduleModal from '../../components/ScheduleModal';


const Schedule = ({handleError, view}) => {
    const [firstDay, setFirstDay] = useState(moment().utc().startOf('week'));
    const [selectedIntervals, setSelectedIntervals] = useState([]);
    const userId = JSON.parse(localStorage.getItem('user')).id;
    const [userRole, setUserRole] = useState('');
    const { instructorId } = useParams();
    const { groupId } = useParams();
    let path = '';
    let url = '';

    switch (view) {
        case 'admin':
            path = '';
            url = '/schedules';
            break;
        case 'instructor':
            path = instructorId;
            url = '/instructor/schedules';
            break; 
        case 'group':
        default:    
            path = groupId;
            url = '/schedules';
    }
    
    useEffect(() => {
        getSchedule(url, path);
    }, [url, path]);

    const getSchedule = (url ,path) => {
        getRequest(url, path, handleError, (response) => {
            console.log(response.data);
            setSelectedIntervals(_.reduce(response.data, (acc, elem) => {
                acc.push({
                    uid: elem.id,
                    start: moment(elem.start),
                    end: moment(elem.end),
                    groupName: `Zajęcia ${elem.group.name}`,
                    value: elem.group.id,
                });
                return acc;
            }, []));
        });
    };

    useEffect(() => {
        getRequest('/users', userId, handleError, (response) => {
            setUserRole(response.data.role.name);
        });
    }, []);

    const handleSelect = (newIntervals) => {
        newIntervals.map((interval) => {
            const newInterval = {
                date: moment.parseZone(interval.start).utc().toJSON(),
                start: moment.parseZone(interval.start).utc().toJSON(),
                end: moment.parseZone(interval.end).utc().toJSON(),
                group: {id: interval.value}
            };

            console.log(newInterval);

            postRequest('/schedules', newInterval, handleError, (response) => {
                if (response.data.group.id === groupId || !groupId) {
                    setSelectedIntervals(selectedIntervals.concat([{
                        uid: response.data.id,
                        start: moment(response.data.start),
                        end: moment(response.data.end),
                        groupName: `Zajęcia ${response.data.group.name}`,
                        value: response.data.group.id,
                    }]));
                }
            });
        });
    };

    const handleEventRemove = (event) => {
        const index = selectedIntervals.findIndex((interval) => interval.uid === event.uid);
        if (index > -1) {
            deleteRequest('/schedules', selectedIntervals[index].uid, handleError, (response) => {
                const newIntervals = _.cloneDeep(selectedIntervals);
                newIntervals.splice(index, 1);
                setSelectedIntervals(newIntervals);
            });
        }
    };
    
    const handleEventUpdate = (event) => {
        const index = selectedIntervals.findIndex((interval) => interval.uid === event.uid);
        if (index > -1) {
            const newInterval = {
                date: moment.parseZone(event.start).utc().toJSON(),
                start: moment.parseZone(event.start).utc().toJSON(),
                end: moment.parseZone(event.end).utc().toJSON(),
                group: {id: event.value}
            };

            postRequest(`/schedules/${selectedIntervals[index].uid}`, newInterval, handleError, (response) => {
                const newIntervals = _.cloneDeep(selectedIntervals);
                newIntervals[index] = {
                    uid: response.data.id,
                    start: moment(response.data.start),
                    end: moment(response.data.end),
                    groupName: `Zajęcia ${response.data.group.name}`,
                    value: response.data.group.id,
                };
                setSelectedIntervals(newIntervals);
            });
        }
    };

    return (<>
        <h1 className="App-page-title">Plan zajęć</h1>
        <div className='Schedule-naviButtons'>
            <Button onClick={() => setFirstDay(moment(firstDay).add(-7, 'day'))}>&lt;</Button>
            <Button onClick={() => setFirstDay(moment().utc().startOf('week'))}>Dziś</Button>
            <Button onClick={() => setFirstDay(moment(firstDay).add(7, 'day'))}>&gt;</Button>
        </div>
        <WeekCalendar
            startTime={moment({h: 9, m: 0})}
            endTime={moment({h: 22, m: 30})}
            numberOfDays={7}
            scaleUnit={30}
            firstDay={firstDay}
            selectedIntervals={selectedIntervals}
            cellHeight={20}
            useModal={['admin', 'instructor'].includes(userRole)}
            onIntervalSelect={handleSelect}
            eventComponent={IntervalEvent}
            onIntervalUpdate={handleEventUpdate}
            onIntervalRemove={handleEventRemove}
            modalComponent={ScheduleModal}
        />
    </>);
}

export default Schedule;