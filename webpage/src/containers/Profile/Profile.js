import { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { getRequest } from '../../lib/communication';
import _ from 'lodash';
import { Link } from 'react-router-dom';

const Profile = ({handleLogout, handleError}) => {
    const [profileDetails, setProfileDetails] = useState(null);
    const userId = JSON.parse(localStorage.getItem('user')).id;

    useEffect(() => {
        getRequest('/users', userId, handleError, (response) => {
            setProfileDetails(response.data);
            console.log(response.data);
        });
    }, []);

    return (<>
        <h1 className="App-page-title">Mój profil</h1>
        <div  className="App-content">
            <div>
                {profileDetails && <div>
                    <div>
                        Imię i nazwisko: <b>{`${profileDetails.firstname} ${profileDetails.lastname}`}</b><br />
                        Email: <b>{profileDetails.email}</b><br />
                        Grupa: <b>{profileDetails.group.name}</b><br />
                    </div>
                    {profileDetails.role.name === 'student' && <>
                        <Link className="App-tiles-button" to={`/schedule/${profileDetails.group.id}`}>Plan zajęć</Link>
                        <Link className="App-tiles-button" to={`/news/group/${profileDetails.group.id}`}>Aktualności w {profileDetails.group.name}</Link>
                        <Link className="App-tiles-button" to={`/blog/group/${profileDetails.group.id}`}>Tablica grupy {profileDetails.group.name}</Link>
                    </>}
                    {profileDetails.role.name === 'admin' && <>
                        <Link className="App-tiles-button" to="/schedule">Plan zajęć</Link>
                        <Link className="App-tiles-button" to="/users">Zarządzaj użytkownikami</Link>
                        <Link className="App-tiles-button" to="/students">Zarządzaj uczniami</Link>
                        <Link className="App-tiles-button" to="/instructors">Zarządzaj instruktorami</Link>
                        <Link className="App-tiles-button" to="/groups">Zarządzaj grupami</Link>
                    </>}
                    {profileDetails.role.name === 'instructor' && <>
                        <Link className="App-tiles-button" to={`/schedule/instructor/${profileDetails.id}`}>Plan zajęć</Link>
                        <Link className="App-tiles-button" to="/students">Zarządzaj uczniami</Link>
                        <Link className="App-tiles-button" to={`/groups/instructor/${profileDetails.id}`}>Zarządzaj grupami</Link>
                    </>}
                    <Link className="App-tiles-button" to={`/users/edit/${profileDetails.id}`}>Edytuj dane konta</Link>
                </div>}
                <Button onClick={handleLogout}>Wyloguj się</Button>
            </div>
        </div>
    </>);
}

export default Profile;