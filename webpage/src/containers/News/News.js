import { getRequest } from '../../lib/communication';
import { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import _ from 'lodash';
import { FaEdit } from 'react-icons/fa';
import { Button } from 'react-bootstrap';

const News = ({handleError}) => {
    const { groupId } = useParams();
    const [news, setNews] = useState(null);
    const [groupInfo, setGroupInfo] = useState(null);
    const [userDetails, setUserDetails] = useState('');
    const history = useHistory();

    useEffect(() => {
        if (groupId) {
            getRequest('/news/group', groupId, handleError, (response) => {
                setNews(response.data);
            });
        } else {
            getRequest('/news', '', handleError, (response) => {
                setNews(response.data);
            });
        }
        
        if (groupId) {
            getRequest('/groups', groupId, handleError, (response) => {
                setGroupInfo(response.data);
            });
        }
    }, [groupId]);

    useEffect(() => {
        const user = JSON.parse(localStorage.getItem('user'));
        
        if (user) {
            getRequest('/users', user.id, handleError, (response) => {
                setUserDetails(response.data);
            });
        }
    }, []);

    const isAdminOrInstructor = () => {
        if (!userDetails) {
            return false;
        } else {
            return ['admin', 'instructor'].includes(userDetails.role.name);
        }
    };

    return (<>
        <h1 className="App-page-title">Aktualności</h1>
        {groupInfo && <h3>Grupa {groupInfo.name}</h3>}
        {isAdminOrInstructor() && <Button onClick={() => {
            if (groupId) {
                history.push(`/news/group/${groupId}/add`);
            } else {
                history.push(`/news/add`);
            }  
        }}>
            Dodaj news
        </Button>}
        <div className="App-content">
            {news && <div className="App-news-list">
                {_.map(news, (elem) => {
                    return (<div className="App-news" key={elem.id}>
                        <div className="App-news-title">
                            {elem.shortDescription} {isAdminOrInstructor() && <span className="App-edit-news-button" onClick={() => {
                                if (groupId) {
                                    history.push(`/news/group/${groupId}/edit/${elem.id}`);
                                } else {
                                    history.push(`/news/edit/${elem.id}`);
                                }
                            }}><FaEdit /></span>}
                        </div>
                        <div className="App-news-date">{elem.date}</div>
                        <div className="App-news-content">{elem.content}</div>
                    </div>);
                })}
            </div>}
        </div>
    </>);
}

export default News;