import { Form, Button, Alert } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { getRequest } from '../../lib/communication';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import { API_ADDRESS } from '../../lib/communication';

const Register = () => {
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [passwordRepeat, setPasswordRepeat] = useState('');
    const [firstname, setFirstname] = useState('');
    const [lastname, setLastname] = useState('');
    const [email, setEmail] = useState('');
    const [groupId, setGroupId] = useState('');
    const [registerStatus, setRegisterStatus] = useState(null);
    const [groups, setGroups] = useState([]);

    useEffect(() => {
        getRequest('/groups', '', (error) => {console.error(error);}, (response) => {
            setGroups(response.data);
        });
    }, []);

    useEffect(() => {
        if (groups.length) {
            setGroupId(groups[0].id);
        }
    }, [groups]);

    const handleSubmit = (event) => {
        event.preventDefault();
        if (password === passwordRepeat) {
            axios.post(`${API_ADDRESS}/register`, {
                username: login,
                password: password,
                firstname: firstname,
                lastname: lastname,
                email: email,
                group: {id: groupId},
                role: {name: 'student'} 
            }).then((response) => {
                if (response.status === 200) {
                    setRegisterStatus('success');
                }
            }, (error) => {
                if (error.response.status === 500) {
                    setRegisterStatus('bad data');
                } else {
                    setRegisterStatus('error');
                }
                console.log(error);
            });
        } else {
            setRegisterStatus('different passwords');
            setPassword('');
            setPasswordRepeat('');
        }
        window.scrollTo(0, 0);
    };

    return (<>
        <h1 className="App-page-title">Zarejestruj się</h1>
        <div className="App-content">
            {registerStatus === 'bad data' && <Alert variant="danger" className="App-register-alert">Wprowadzono błędne dane, podany login lub email został już użyty.</Alert>}
            {registerStatus === 'different passwords' && <Alert variant="danger" className="App-register-alert">Podane hasła są różne.</Alert>}
            {registerStatus === 'error' && <Alert variant="warning" className="App-register-alert">Wystąpił problem, spróbuj ponownie później.</Alert>}
            {registerStatus !== 'success' && <Form onSubmit={handleSubmit} className="App-register-form">
                <Form.Group controlId="formRegisterFirstname">
                    <Form.Label>Imię</Form.Label>
                    <Form.Control type="text" value={firstname} onChange={event => setFirstname(event.target.value)} placeholder="Imię" required />
                </Form.Group>
                <Form.Group controlId="formRegisterLastname">
                    <Form.Label>Nazwisko</Form.Label>
                    <Form.Control type="text" value={lastname} onChange={event => setLastname(event.target.value)} placeholder="Nazwisko" required />
                </Form.Group>
                <Form.Group controlId="formRegisterEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control type="email" value={email} onChange={event => setEmail(event.target.value)} placeholder="Email" required />
                </Form.Group>
                <Form.Group controlId="formRegisterLogin">
                    <Form.Label>Login</Form.Label>
                    <Form.Control type="text" value={login} onChange={event => setLogin(event.target.value)} placeholder="Login" required />
                </Form.Group>
                <Form.Group controlId="formRegisterPassword">
                    <Form.Label>Hasło</Form.Label>
                    <Form.Control type="password" value={password} onChange={event => setPassword(event.target.value)} placeholder="Hasło" required />
                </Form.Group>
                <Form.Group controlId="formRegisterPasswordRepeat">
                    <Form.Label>Powtórz hasło</Form.Label>
                    <Form.Control type="password" value={passwordRepeat} onChange={event => setPasswordRepeat(event.target.value)} placeholder="Powtórz hasło" required />
                </Form.Group>
                <Form.Group controlId="formRegisterGroupSelect">
                    <Form.Label>Wybierz interesującą Cię grupę</Form.Label>
                    {groups.length && <Form.Control as="select" value={groupId} onChange={(event) => setGroupId(event.target.value)}>
                        {_.map(groups, (group) => (<option key={group.id} value={group.id}>{group.name}</option>))}
                    </Form.Control>}
                </Form.Group>
                <Button variant="primary" type="submit">
                    Załóż konto
                </Button>
            </Form>}
            {registerStatus === 'success' && <div>
                Rejestracja przebiegła pomyślnie. <br />
                Dziękujemy za założenie konta, kliknij w poniższy przycisk w celu zalogowania się na konto. <br />
                <Button as={Link} to='/profile'>Zaloguj się</Button>
            </div>}
        </div>
    </>);
}

export default Register;