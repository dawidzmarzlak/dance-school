const Home = () => {
    return (<>
        <h1 className="App-page-title">Witamy na stronie Szkoły tańca</h1>
        <div className="App-content">
            <div><img src="home.jpg" alt="" height={300} /></div>
            <div>
                <p>
                    Jesteśmy renomowaną szkołą tańca w Warszawie. Jesteśmy na rynku od ponad 10 lat.
                    Nasze lekcje przeznaczone są dla róznych grup wiekowych.
                </p>
                <p>
                    W naszej ofercie znajduje się wiele różnych form tańca. Napewno kazdy znajdzie coś dla siebie.
                    Zapraszamy do skorzystania z naszej oferty i założenia konta na naszym portalu.
                </p>
                <p>
                    Założenie konta umożliwi Państwu dostęp do aktualnści skierowanych do konketnych grup szkoleniowych.
                    Zarejestrowani użytkowiny mają również dostęp do planu zajęć prezentowanego w bardzo przejrzysty sposób.
                </p>
                <p>
                    <b>Zachęcamy również do pobrania naszej dedykowanej apliakcji na telefon</b>
                </p>
            </div>
        </div>
    </>);
}

export default Home;