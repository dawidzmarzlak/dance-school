import { useEffect, useState } from 'react';
import { Row } from 'react-bootstrap';
import { getRequest } from '../../lib/communication';
import _ from 'lodash';
import { useHistory, useParams } from 'react-router-dom';
import { userRoles } from '../../lib/user';

const UserList = ({handleError, getUrl, title}) => {
    const [userList, setUserList] = useState([]);
    const history = useHistory();
    const { groupId } = useParams();

    useEffect(() => {
        const getPathParams = groupId || '';
        getRequest(getUrl, getPathParams, handleError, (response) => {
            setUserList(response.data);
        });
    }, []);

    return (<>
        <h1 className="App-page-title">{title}</h1>
        <div  className="App-content">
            <div className="App-user-list">
                {_.map(userList, (user) => 
                    <Row key={user.id} className="App-user-row" onClick={() => history.push(`/users/edit/${user.id}`)}>
                        <div style={{width: '100%', textAlign: 'left'}}>
                            <div className="App-user-role">{userRoles[user.role.name]}</div>
                            <div>{user.firstname} {user.lastname} ({user.email})</div>
                        </div>
                    </Row>
                )}
            </div>
        </div>
    </>);
}

export default UserList;