import { useState, useEffect } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import _ from 'lodash';
import { getRequest } from '../lib/communication';

const ScheduleModal = ({start, end, value, onRemove, onSave}) => {
    const [groupId, setGroupId] = useState(value);
    const [groups, setGroups] = useState([]);

    useEffect(() => {
        getRequest('/groups', '', (error) => {console.error(error);}, (response) => {
            setGroups(response.data);
            if (!groupId) {
                setGroupId(response.data[0].id);
            }
        });
    }, []);

    const handleRemove = () => {
        onRemove();
    };

    const handleSave = () => {
        onSave({
            value: groupId,
        });
    };

    const renderText = () => {
        if (start.isSame(end, 'day')) {
            return (<span>{`${start.format('Do MMM., HH:mm')} - ${end.format('HH:mm')}`}</span>);
        }
        return (<span>{`${start.format('Do MMM.')} - ${end.format('Do MMM.')}, ${start.format('HH:mm')} - ${end.format('HH:mm')}`}</span>);
    }

    return (
        <Modal.Dialog>
            <Modal.Header><Modal.Title>{renderText()}</Modal.Title></Modal.Header>
            <Modal.Body>
                <Form.Group controlId="groupSelect">
                    <Form.Label>Wybierz grupę</Form.Label>
                    {groups.length && <Form.Control as="select" value={groupId} onChange={(event) => setGroupId(event.target.value)}>
                        {_.map(groups, (group) => (<option key={group.id} value={group.id}>{group.name}</option>))}
                    </Form.Control>}
                </Form.Group>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={handleRemove}>Usuń</Button>
                <Button onClick={handleSave}>Zapisz</Button>
            </Modal.Footer>
        </Modal.Dialog>
    );
};

export default ScheduleModal;