
const Footer = () => {
    return (
        <footer className="App-footer">
            <div className="App-footer-Brand">
                <h2>Szkoła tańca</h2>
            </div>
            Szkoła tańca &copy; Copyright 2020 | All Rights Reserved
        </footer>
    );
}

export default Footer;