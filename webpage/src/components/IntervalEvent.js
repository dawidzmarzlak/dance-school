import { Tooltip, OverlayTrigger } from 'react-bootstrap';

const IntervalEvent = ({start, end, groupName}) => {
    const renderTooltip = (props) => (
        <Tooltip id="button-tooltip" {...props}>
            {groupName}<br />
            {`${start.format('HH:mm')} - ${end.format('HH:mm')}`}
        </Tooltip>
    );

    return (
        <OverlayTrigger
            placement="bottom"
            delay={{ show: 250, hide: 400 }}
            overlay={renderTooltip}
        >
            <div className="IntervalEvent">
                <span>{`${start.format('HH:mm')} - ${end.format('HH:mm')}`}</span>
                <br />
                {groupName}
            </div>
        </OverlayTrigger>
    );
}

export default IntervalEvent;