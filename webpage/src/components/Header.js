import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Header = ({logged}) => {
    return (
        <header className="App-header">
            <Navbar collapseOnSelect expand="md" bg="primary" variant="dark" sticky="top">
                <Navbar.Brand as={Link} to="/">
                    <img
                        alt=""
                        src="/logo.png"
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                    />{' '}
                    Szkoła tańca
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link eventKey="1" as={Link} to="/">Home</Nav.Link>
                        <Nav.Link eventKey="2" as={Link} to="/contact">Kontakt</Nav.Link>
                        <Nav.Link eventKey="3" as={Link} to="/news">Aktualności</Nav.Link>
                        {!logged && <Nav.Link eventKey="4" as={Link} to="/register">Rejestracja</Nav.Link>}
                        <Nav.Link eventKey="5" as={Link} to="/profile">{logged ? 'Profil' : 'Zaloguj się'}</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
      </header>
    );
}

export default Header;