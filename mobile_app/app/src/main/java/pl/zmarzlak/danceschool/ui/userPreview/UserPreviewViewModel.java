package pl.zmarzlak.danceschool.ui.userPreview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import pl.zmarzlak.danceschool.models.User;

public class UserPreviewViewModel extends ViewModel {
    private MutableLiveData<User> mUser;

    public UserPreviewViewModel() {
        mUser = new MutableLiveData<>();
        mUser.setValue(null);
    }

    public LiveData<User> getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser.postValue(user);
    }
}
