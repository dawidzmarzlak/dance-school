package pl.zmarzlak.danceschool.ui.register;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.LinkedList;
import java.util.List;

import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.User;

public class RegisterViewModel extends ViewModel {

    private MutableLiveData<List<Group>> mGroups;

    public RegisterViewModel() {
        mGroups = new MutableLiveData<>();
        mGroups.setValue(new LinkedList<>());
    }

    public LiveData<List<Group>> getGroups() {
        return mGroups;
    }

    public void setGroups(List<Group> groups) {
        mGroups.postValue(groups);
    }
}

