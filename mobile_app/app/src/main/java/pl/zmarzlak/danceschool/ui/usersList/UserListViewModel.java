package pl.zmarzlak.danceschool.ui.usersList;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.view.NestedScrollingChild;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.News;
import pl.zmarzlak.danceschool.models.User;

public class UserListViewModel extends RecyclerView.Adapter<UserListViewModel.ViewHolder> {

    private List<User> usersList;
    private View view;

    public void setUsersList(List<User> usersList) {
        this.usersList = usersList;
    }

    public UserListViewModel(View view, List<User> usersList) {
        this.usersList = usersList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View userListElem = inflater.inflate(R.layout.user_list_elem, parent, false);
        ViewHolder viewHolder = new ViewHolder(userListElem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user = usersList.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putLong("userId", user.getId());
                Navigation.findNavController(view).navigate(R.id.userPreviewFragment, bundle);
            }
        });


        TextView role = holder.role;
        role.setText(user.getRole().toString());

        TextView name = holder.name;
        name.setText(user.getFirstname() + " " + user.getLastname() + "(" + user.getEmail() + ")");
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView role;
        private TextView name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            role = (TextView) itemView.findViewById(R.id.user_role_elem);
            name = (TextView) itemView.findViewById(R.id.user_name_elem);
        }
    }

}