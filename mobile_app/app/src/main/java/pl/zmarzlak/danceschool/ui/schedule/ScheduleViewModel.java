package pl.zmarzlak.danceschool.ui.schedule;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.LinkedList;
import java.util.List;

import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.Schedule;

public class ScheduleViewModel extends ViewModel {
    private MutableLiveData<List<Schedule>> mSchedules;
    private MutableLiveData<List<Group>> mGroups;

    public ScheduleViewModel() {
        mSchedules = new MutableLiveData<>();
        mSchedules.setValue(new LinkedList<>());
        mGroups = new MutableLiveData<>();
        mGroups.setValue(new LinkedList<>());
    }

    public LiveData<List<Schedule>> getSchedules() {
        return mSchedules;
    }

    public void setSchedules(List<Schedule> schedules) {
        mSchedules.postValue(schedules);
    }

    public LiveData<List<Group>> getGroups() {
        return mGroups;
    }

    public void setGroups(List<Group> groups) {
        mGroups.postValue(groups);
    }
}
