package pl.zmarzlak.danceschool.ui.login;

import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.hotspot2.pps.Credential;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.AuthRequest;
import pl.zmarzlak.danceschool.models.TokenResponse;
import pl.zmarzlak.danceschool.service.APIService;
import pl.zmarzlak.danceschool.service.RetrofitClient;
import pl.zmarzlak.danceschool.service.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends Fragment {

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }
    private SessionManager sessionManager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.login_fragment, container, false);
        Context context = root.getContext();
        sessionManager = new SessionManager(context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE));

        ((Button) root.findViewById(R.id.button_login)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleLogin(root);
            }
        });

        return root;
    }

    public void handleLogin(View view) {
        String login = ((EditText) view.findViewById(R.id.login)).getText().toString();
        String password = ((EditText) view.findViewById(R.id.password)).getText().toString();
        authWithApi(login, password);
    }

    private void authWithApi(String login, String password) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.authenticate(new AuthRequest(login, password)).enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                if (response.isSuccessful()) {
                    boolean status = sessionManager.saveCredentials(login, password, response.body().getToken(), response.body().getId());
                    if (status) {
                        Navigation.findNavController(getActivity().getCurrentFocus()).navigate(R.id.navigation_profile);
                    } else {
                        handleError();
                    }
                } else {
                    Toast.makeText(getContext(), getContext().getString(R.string.bad_login), Toast.LENGTH_LONG).show();
                    ((EditText) getView().findViewById(R.id.password)).setText("");
                }
            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t) {
                Log.i("ERR", "error with connection");
                handleError();
            }
        });
    }

    private void handleError() {
        Toast.makeText(getContext(), getContext().getString(R.string.login_error), Toast.LENGTH_LONG).show();
    }
}
