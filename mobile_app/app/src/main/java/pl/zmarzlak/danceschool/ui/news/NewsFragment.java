package pl.zmarzlak.danceschool.ui.news;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.News;
import pl.zmarzlak.danceschool.models.User;
import pl.zmarzlak.danceschool.service.APIService;
import pl.zmarzlak.danceschool.service.RetrofitClient;
import pl.zmarzlak.danceschool.service.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsFragment extends Fragment {

    private NewsViewModel newsViewModel;
    private List<News> newsList = new LinkedList<>();
    private SessionManager sessionManager;
    private Group group;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_news, container, false);
        Context context = root.getContext();
        sessionManager = new SessionManager(context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE));
        Bundle bundle = getArguments();
        Long groupId = -1L;
        if (Objects.isNull(bundle)) {
            loadNewsFromApi();
        } else {
            if (!sessionManager.isUserAuthorized()) {
                Navigation.findNavController(root).navigate(R.id.loginFragment);
            }
            groupId = bundle.getLong("groupId");
            loadGroupData(groupId);
            loadNewsForGroupFromApi(groupId);
        }
        loadUserData(root, groupId);
        newsViewModel = new NewsViewModel(newsList, root, groupId);
        RecyclerView recyclerView = root.findViewById(R.id.news_list_recycler_view);
        recyclerView.setAdapter(newsViewModel);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));

        return root;
    }

    private void loadNewsForGroupFromApi(Long groupId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getNewsForGroup("Bearer " + sessionManager.getUserToken(), groupId).enqueue(new Callback<List<News>>() {
            @Override
            public void onResponse(Call<List<News>> call, Response<List<News>> response) {
                if (response.isSuccessful()) {
                    newsViewModel.setNewsList(response.body());
                    newsViewModel.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<News>> call, Throwable t) {
                Log.e("tag", "error API");
            }
        });
    }

    private void loadUserData(View root, Long groupId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getUserInfo("Bearer " + sessionManager.getUserToken(), sessionManager.getUserId()).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    boolean isInstructorOfGroup = false;
                    if (!Objects.isNull(group)) {
                        isInstructorOfGroup = "instructor".equals(response.body().getRole().getName()) && response.body().getId() == group.getInstructorId();
                    }
                    boolean isAdminView = "admin".equals(response.body().getRole().getName()) || isInstructorOfGroup;
                    newsViewModel.setAdminView(isAdminView);
                    newsViewModel.notifyDataSetChanged();
                    if (isAdminView) {
                        LinearLayout newsContainer = root.findViewById(R.id.news_container);
                        Button button = new Button(getContext());
                        button.setText(getContext().getString(R.string.add_news));
                        button.setPadding(24, 8, 24, 8);
                        button.setWidth(70);
                        button.setHeight(35);
                        button.setOnClickListener(new Button.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Bundle bundle = new Bundle();
                                bundle.putString("mode", "add");
                                bundle.putLong("groupId", groupId);
                                Navigation.findNavController(root).navigate(R.id.addNewsFragment, bundle);
                            }
                        });
                        newsContainer.addView(button);
                    }
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }

    private void loadGroupData(Long groupId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getGroupInfo("Bearer " + sessionManager.getUserToken(), groupId).enqueue(new Callback<Group>() {
            @Override
            public void onResponse(Call<Group> call, Response<Group> response) {
                if (response.isSuccessful()) {
                    group = response.body();
                }
            }

            @Override
            public void onFailure(Call<Group> call, Throwable t) {

            }
        });
    }

    private void loadNewsFromApi() {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getNewsForAll().enqueue(new Callback<List<News>>() {
            @Override
            public void onResponse(Call<List<News>> call, Response<List<News>> response) {
                if (response.isSuccessful()) {
                    newsViewModel.setNewsList(response.body());
                    newsViewModel.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<News>> call, Throwable t) {
                Log.e("tag", "error API");
            }
        });
    }
}
