package pl.zmarzlak.danceschool.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class BlogPost {
    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("date")
    @Expose
    private Date date;

    @SerializedName("content")
    @Expose
    private String content;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("votes")
    @Expose
    private List<Vote> votes;

    @SerializedName("group")
    @Expose
    private Group group;

    @SerializedName("user")
    @Expose
    private User user;

    public BlogPost() {
    }

    public BlogPost(String content, String image, Group group, User user) {
        this.content = content;
        this.image = image;
        this.group = group;
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Vote> getVotes() {
        return votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "BlogPost{" +
                "id=" + id +
                ", date=" + date +
                ", content='" + content + '\'' +
                ", image='" + image + '\'' +
                ", votes=" + votes +
                ", group=" + group +
                ", user=" + user +
                '}';
    }
}
