package pl.zmarzlak.danceschool.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Vote {
    @SerializedName("id")
    @Expose
    private Long id;

    @SerializedName("user")
    @Expose
    private Long user;

    @SerializedName("blogPost")
    @Expose
    private Long blogPost;

    public Vote() {
    }

    public Vote(Long user, Long blogPost) {
        this.user = user;
        this.blogPost = blogPost;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUser() {
        return user;
    }

    public void setUser(Long user) {
        this.user = user;
    }

    public Long getBlogPost() {
        return blogPost;
    }

    public void setBlogPost(Long blogPost) {
        this.blogPost = blogPost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vote vote = (Vote) o;
        return user.equals(vote.user) &&
                blogPost.equals(vote.blogPost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, blogPost);
    }

    @Override
    public String toString() {
        return "Vote{" +
                "id=" + id +
                ", user=" + user +
                ", blogPost=" + blogPost +
                '}';
    }
}
