package pl.zmarzlak.danceschool.ui.groupPreview;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.User;
import pl.zmarzlak.danceschool.service.APIService;
import pl.zmarzlak.danceschool.service.RetrofitClient;
import pl.zmarzlak.danceschool.service.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupPreviewFragment extends Fragment {

    private GroupPreviewViewModel groupPreviewViewModel;
    private SessionManager sessionManager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.group_preview_fragment, container, false);
        Context context = root.getContext();
        sessionManager = new SessionManager(context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE));

        if (!sessionManager.isUserAuthorized()) {
            Navigation.findNavController(container).navigate(R.id.loginFragment);
        }

        Long groupId = getArguments().getLong("groupId");

        groupPreviewViewModel = ViewModelProviders.of(this).get(GroupPreviewViewModel.class);
        final TextView groupName = root.findViewById(R.id.group_name_preview);
        final TextView groupInstructor = root.findViewById(R.id.group_instructor_preview);

        ((Button) root.findViewById(R.id.button_edit_group)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putLong("groupId", groupId);
                bundle.putString("mode", "edit");
                Navigation.findNavController(root).navigate(R.id.editGroupFragment, bundle);
            }
        });

        ((Button) root.findViewById(R.id.button_display_group_news)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putLong("groupId", groupId);
                bundle.putString("title", getContext().getString(R.string.news_for_group) + " "
                        + groupPreviewViewModel.getGroup().getValue().getName());
                Navigation.findNavController(root).navigate(R.id.navigation_news_for_group, bundle);
            }
        });

        ((Button) root.findViewById(R.id.button_display_group_students)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putLong("groupId", groupId);
                bundle.putString("mode", "groupStudents");
                Navigation.findNavController(root).navigate(R.id.groupStudentsListFragment, bundle);
            }
        });

        ((Button) root.findViewById(R.id.button_display_group_schedule)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putLong("groupId", groupId);
                bundle.putString("mode", "group");
                bundle.putString("userPriviliges", "edit");
                Navigation.findNavController(root).navigate(R.id.scheduleFragment, bundle);
            }
        });

        ((Button) root.findViewById(R.id.button_display_group_blog)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putLong("groupId", groupId);
                bundle.putLong("userId", sessionManager.getUserId());
                bundle.putString("title", getContext().getString(R.string.blog_button) + " "
                        + groupPreviewViewModel.getGroup().getValue().getName());
                Navigation.findNavController(root).navigate(R.id.blogFragment, bundle);
            }
        });

        ((Button) root.findViewById(R.id.button_delete_group)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setCancelable(true);
                builder.setTitle(getContext().getText(R.string.delete_group));
                builder.setMessage(getContext().getText(R.string.delete_group_msg));
                builder.setPositiveButton(getContext().getText(R.string.confirm),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handleDelete(root, groupId);
                            }
                        });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        groupPreviewViewModel.getGroup().observe(getViewLifecycleOwner(), new Observer<Group>() {
            @Override
            public void onChanged(@Nullable Group group) {
                if (!Objects.isNull(group)) {
                    groupName.setText(group.getName());
                    getInstructorData(group.getInstructorId());
                }
            }
        });

        groupPreviewViewModel.getInstructor().observe(getViewLifecycleOwner(), new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                if (!Objects.isNull(user)) {
                    groupInstructor.setText(user.getFirstname() + " " + user.getLastname());
                }
            }
        });

        getGroupData(groupId);
        return root;
    }

    private void handleDelete(View root, Long groupId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.deleteGroup("Bearer " + sessionManager.getUserToken(), groupId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Navigation.findNavController(root).popBackStack();
                    Toast.makeText(getContext(), getContext().getText(R.string.delete_group_successful), Toast.LENGTH_LONG).show();
                } else {
                    handleError();
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                handleError();
            }
        });
    }

    private void handleError() {
        Toast.makeText(getContext(), getContext().getText(R.string.error), Toast.LENGTH_LONG).show();

    }

    private void getGroupData(Long groupId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getGroupInfo("Bearer " + sessionManager.getUserToken(), groupId).enqueue(new Callback<Group>() {
            @Override
            public void onResponse(Call<Group> call, Response<Group> response) {
                if (response.isSuccessful()) {
                    groupPreviewViewModel.setGroup(response.body());
                }

            }

            @Override
            public void onFailure(Call<Group> call, Throwable t) {

            }
        });
    }

    private void getInstructorData(Long userId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getUserInfo("Bearer " + sessionManager.getUserToken(), userId).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    groupPreviewViewModel.setInstructor(response.body());
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }

}
