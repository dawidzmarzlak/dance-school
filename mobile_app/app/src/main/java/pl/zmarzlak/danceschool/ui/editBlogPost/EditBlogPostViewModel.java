package pl.zmarzlak.danceschool.ui.editBlogPost;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import pl.zmarzlak.danceschool.models.BlogPost;

public class EditBlogPostViewModel extends ViewModel {
    private MutableLiveData<BlogPost> mBlogPost;
    private MutableLiveData<String> mImage;

    public EditBlogPostViewModel() {
        mBlogPost = new MutableLiveData<>();
        mBlogPost.setValue(null);
        mImage = new MutableLiveData<>();
        mImage.setValue("");
    }

    public LiveData<BlogPost> getBlogPost() {
        return mBlogPost;
    }

    public void setBlogPost(BlogPost blogPost) {
        mBlogPost.postValue(blogPost);
    }

    public MutableLiveData<String> getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage.postValue(image);
    }
}
