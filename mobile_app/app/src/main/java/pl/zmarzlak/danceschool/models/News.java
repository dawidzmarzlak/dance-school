package pl.zmarzlak.danceschool.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class News {
    @SerializedName("id")
    @Expose
    private Long id;

    @SerializedName("shortDescription")
    @Expose
    private String title;

    @SerializedName("content")
    @Expose
    private String content;

    @SerializedName("date")
    @Expose
    private Date date;

    @SerializedName("group")
    @Expose
    private Group group;

    public News() {
    }

    public News(Long id, String title, String content, Date date, Group group) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.date = date;
        this.group = group;
    }

    public News(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "News{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", date=" + date +
                ", group=" + group +
                '}';
    }
}
