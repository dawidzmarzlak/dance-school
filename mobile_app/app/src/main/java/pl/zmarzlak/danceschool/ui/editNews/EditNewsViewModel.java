package pl.zmarzlak.danceschool.ui.editNews;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import pl.zmarzlak.danceschool.models.News;


public class EditNewsViewModel extends ViewModel {
    private MutableLiveData<News> mNews;

    public EditNewsViewModel() {
        mNews = new MutableLiveData<>();
        mNews.setValue(null);
    }

    public LiveData<News> getNews() {
        return mNews;
    }

    public void setNews(News news) {
        mNews.postValue(news);
    }
}
