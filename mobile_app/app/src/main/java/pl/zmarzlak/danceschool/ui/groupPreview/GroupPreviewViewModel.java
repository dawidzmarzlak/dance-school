package pl.zmarzlak.danceschool.ui.groupPreview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.User;

public class GroupPreviewViewModel extends ViewModel {
    private MutableLiveData<User> mInstructor;
    private MutableLiveData<Group> mGroup;

    public GroupPreviewViewModel() {
        mInstructor = new MutableLiveData<>();
        mInstructor.setValue(null);
        mGroup = new MutableLiveData<>();
        mGroup.setValue(null);
    }

    public LiveData<User> getInstructor() {
        return mInstructor;
    }

    public void setInstructor(User user) {
        mInstructor.postValue(user);
    }

    public LiveData<Group> getGroup() {
        return mGroup;
    }

    public void setGroup(Group group) {
        mGroup.postValue(group);
    }
}
