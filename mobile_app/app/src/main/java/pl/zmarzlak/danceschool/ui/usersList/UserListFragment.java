package pl.zmarzlak.danceschool.ui.usersList;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.News;
import pl.zmarzlak.danceschool.models.User;
import pl.zmarzlak.danceschool.service.APIService;
import pl.zmarzlak.danceschool.service.RetrofitClient;
import pl.zmarzlak.danceschool.service.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserListFragment extends Fragment {

    private UserListViewModel usersViewModel;
    private List<User> usersList = new LinkedList<>();
    private SessionManager sessionManager;
    private Bundle bundle;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.user_list_fragment, container, false);
        Context context = root.getContext();
        sessionManager = new SessionManager(context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE));
        bundle = getArguments();
        if (!sessionManager.isUserAuthorized()) {
            Navigation.findNavController(root).navigate(R.id.loginFragment);
        }
        String mode = bundle.getString("mode");
//        Long groupId = bundle.getLong("groupId");
        loadUsersFromApi(mode);

        usersViewModel = new UserListViewModel(root, usersList);
        RecyclerView recyclerView = root.findViewById(R.id.users_list_recycler_view);
        recyclerView.setAdapter(usersViewModel);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));

        return root;
    }

    private void loadUsersFromApi(String mode) {
        switch (mode) {
            case "all":
                loadAllUsers();
                break;
            case "students":
                loadAllStudents();
                break;
            case "instructors":
                loadAllInstrucotrs();
                break;
            case "groupStudents":
                Long groupId = bundle.getLong("groupId");
                loadGroupStudents(groupId);
                break;
            default:
                break;
        }
    }

    private void loadAllUsers() {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getAllUsers("Bearer " + sessionManager.getUserToken()).enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.isSuccessful()) {
                    usersViewModel.setUsersList(response.body());
                    usersViewModel.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.e("tag", "error API");
            }
        });
    }

    private void loadAllStudents() {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getAllStudents("Bearer " + sessionManager.getUserToken()).enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.isSuccessful()) {
                    usersViewModel.setUsersList(response.body());
                    usersViewModel.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.e("tag", "error API");
            }
        });
    }

    private void loadGroupStudents(Long groupId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getGroupStudents("Bearer " + sessionManager.getUserToken(), groupId).enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.isSuccessful()) {
                    usersViewModel.setUsersList(response.body());
                    usersViewModel.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.e("tag", "error API");
            }
        });
    }

    private void loadAllInstrucotrs() {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getAllInstructors("Bearer " + sessionManager.getUserToken()).enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.isSuccessful()) {
                    usersViewModel.setUsersList(response.body());
                    usersViewModel.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.e("tag", "error API");
            }
        });
    }
}
