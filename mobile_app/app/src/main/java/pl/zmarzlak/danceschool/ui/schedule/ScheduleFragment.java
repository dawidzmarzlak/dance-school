package pl.zmarzlak.danceschool.ui.schedule;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.RectF;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.Schedule;
import pl.zmarzlak.danceschool.service.APIService;
import pl.zmarzlak.danceschool.service.RetrofitClient;
import pl.zmarzlak.danceschool.service.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScheduleFragment extends Fragment {

    private ScheduleViewModel scheduleViewModel;
    private SessionManager sessionManager;
    private WeekView mWeekView;
    private ArrayAdapter<Group> groupArrayAdapter;
    private ArrayAdapter<String> durationArrayAdapter;

    public static ScheduleFragment newInstance() {
        return new ScheduleFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.schedule_fragment, container, false);

        Context context = root.getContext();
        sessionManager = new SessionManager(context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE));

        if (!sessionManager.isUserAuthorized()) {
            Navigation.findNavController(container).navigate(R.id.loginFragment);
        }

        scheduleViewModel = ViewModelProviders.of(this).get(ScheduleViewModel.class);

        String mode = getArguments().getString("mode");
        String userPriviliges = getArguments().getString("userPriviliges");

        durationArrayAdapter = new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item,
                new String[] {"00:30", "01:00", "01:30", "02:00", "02:30", "03:00", "03:30", "04:00"});

        updateScheduleData(mode);
        getGroups();

        mWeekView = (WeekView) root.findViewById(R.id.weekView);

        mWeekView.setHourHeight(25);
        mWeekView.setOverlappingEventGap(5);

        mWeekView.setOnEventClickListener(new WeekView.EventClickListener() {
            @Override
            public void onEventClick(WeekViewEvent event, RectF eventRect) {
                if ("edit".equals(userPriviliges)) {
                    showEditScheduleDialog(event, mode);
                } else {
                    String startDate = new SimpleDateFormat("HH:mm").format(event.getStartTime().getTime());
                    String endDate = new SimpleDateFormat("HH:mm").format(event.getEndTime().getTime());
                    Toast.makeText(getContext(), event.getName() + " " + startDate + " - " + endDate, Toast.LENGTH_LONG).show();
                }
            }
        });

        mWeekView.setMonthChangeListener(new MonthLoader.MonthChangeListener() {
            @Override
            public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
                LinkedList<WeekViewEvent> monthEvents = new LinkedList<>();
                scheduleViewModel.getSchedules().getValue().stream()
                        .filter(schedule -> {
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(schedule.getDate());
                            return cal.get(Calendar.YEAR) == newYear && cal.get(Calendar.MONTH) == newMonth - 1;
                        })
                        .forEach(schedule -> {
                            Calendar startDate = Calendar.getInstance();
                            startDate.setTime(schedule.getStart());
                            Calendar endDate = Calendar.getInstance();
                            endDate.setTime(schedule.getEnd());
                            monthEvents.add(new WeekViewEvent(schedule.getId(),
                                    getContext().getString(R.string.schedule_title) + schedule.getGroup().getName(),
                                    startDate, endDate));
                        });
                return monthEvents;
            }
        });

        mWeekView.setEventLongPressListener(new WeekView.EventLongPressListener() {
            @Override
            public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setCancelable(true);
                builder.setTitle(getContext().getText(R.string.delete_schedule));
                builder.setMessage(getContext().getText(R.string.delete_schedule_msg));
                builder.setPositiveButton(getContext().getText(R.string.confirm),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handleDelete(event.getId(), mode);
                            }
                        });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        mWeekView.setEmptyViewLongPressListener(new WeekView.EmptyViewLongPressListener() {
            @Override
            public void onEmptyViewLongPress(Calendar time) {
                if ("edit".equals(userPriviliges)) {
                    int unroundedMinutes = time.get(Calendar.MINUTE);
                    int mod = unroundedMinutes % 30;
                    time.add(Calendar.MINUTE, mod < 4 ? -mod : (30 - mod));
                    time.set(Calendar.SECOND, 0);
                    time.set(Calendar.MILLISECOND, 0);
                    showAddScheduleDialog(time, mode);
                }
            }
        });

        scheduleViewModel.getSchedules().observe(getViewLifecycleOwner(), new Observer<List<Schedule>>() {
            @Override
            public void onChanged(List<Schedule> schedules) {
                if (schedules.size() > 0) {
                    mWeekView.notifyDatasetChanged();
                }
            }
        });

        scheduleViewModel.getGroups().observe(getViewLifecycleOwner(), new Observer<List<Group>>() {
            @Override
            public void onChanged(List<Group> groups) {
                Group [] groupsArray = new Group[groups.size()];
                groups.toArray(groupsArray);
                groupArrayAdapter = new ArrayAdapter<Group>(getContext(), R.layout.support_simple_spinner_dropdown_item, groupsArray);
            }
        });

        return root;
    }

    private void updateSchedule(Long scheduleId, Schedule newSchedule, String mode) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.updateSchedule("Bearer " + sessionManager.getUserToken(), scheduleId, newSchedule).enqueue(new Callback<Schedule>() {
            @Override
            public void onResponse(Call<Schedule> call, Response<Schedule> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), getContext().getText(R.string.edit_success), Toast.LENGTH_LONG).show();
                    updateScheduleData(mode);
                } else {
                    handleError();
                }
            }

            @Override
            public void onFailure(Call<Schedule> call, Throwable t) {
                handleError();
            }
        });
    }

    private void showAddScheduleDialog(Calendar time, String mode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.edit_schedule_dialog, null);
        builder.setView(dialogView);
        TextView dialogTitle = dialogView.findViewById(R.id.dialog_title);
        dialogTitle.setText(getContext().getString(R.string.add_schedule));
        TextView startSchedule = dialogView.findViewById(R.id.schedule_start);
        String dateString = new SimpleDateFormat("HH:mm").format(time.getTime());
        startSchedule.setText(getContext().getString(R.string.schedule_start) + dateString);
        Spinner durationSpinner = dialogView.findViewById(R.id.duration_spinner);
        durationSpinner.setAdapter(durationArrayAdapter);
        Spinner groupSpinner = dialogView.findViewById(R.id.schedule_group_spinner);
        groupSpinner.setAdapter(groupArrayAdapter);
        builder.setCancelable(true);
        builder.setPositiveButton(getContext().getText(R.string.save),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String []durationAfterSplit = ((String) durationSpinner.getSelectedItem()).split(":");
                        Date endDate = new Date(time.getTime().getTime() +
                                (Integer.parseInt(durationAfterSplit[0]) * 60 + Integer.parseInt(durationAfterSplit[1])) * 60 * 1000);
                        Schedule newSchedule = new Schedule((Group) groupSpinner.getSelectedItem(),
                                time.getTime(), time.getTime(), endDate);
                        createSchedule(newSchedule, mode);
                    }
                });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showEditScheduleDialog(WeekViewEvent event, String mode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.edit_schedule_dialog, null);
        builder.setView(dialogView);
        TextView dialogTitle = dialogView.findViewById(R.id.dialog_title);
        dialogTitle.setText(getContext().getString(R.string.edit_schedule));
        TextView startSchedule = dialogView.findViewById(R.id.schedule_start);
        String dateString = new SimpleDateFormat("HH:mm").format(event.getStartTime().getTime());
        startSchedule.setText(getContext().getString(R.string.schedule_start) + dateString);
        Spinner durationSpinner = dialogView.findViewById(R.id.duration_spinner);
        durationSpinner.setAdapter(durationArrayAdapter);
        String durationString = new SimpleDateFormat("HH:mm").format(event.getEndTime().getTime().getTime() - event.getStartTime().getTime().getTime() - 60 * 60 * 1000);
        durationSpinner.setSelection(durationArrayAdapter.getPosition(durationString));
        Spinner groupSpinner = dialogView.findViewById(R.id.schedule_group_spinner);
        groupSpinner.setAdapter(groupArrayAdapter);
        int selectGroupPosition = IntStream.range(0, scheduleViewModel.getGroups().getValue().size())
                .filter(i -> (getContext().getString(R.string.schedule_title) + scheduleViewModel.getGroups().getValue().get(i).getName()).equals(event.getName()))
                .findFirst()
                .orElse(-1);
        groupSpinner.setSelection(selectGroupPosition);
        builder.setCancelable(true);
        builder.setPositiveButton(getContext().getText(R.string.save),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String []durationAfterSplit = ((String) durationSpinner.getSelectedItem()).split(":");
                        Date endDate = new Date(event.getStartTime().getTime().getTime() +
                                (Integer.parseInt(durationAfterSplit[0]) * 60 + Integer.parseInt(durationAfterSplit[1])) * 60 * 1000);
                        Schedule newSchedule = new Schedule((Group) groupSpinner.getSelectedItem(),
                                event.getStartTime().getTime(), event.getStartTime().getTime(), endDate);
                        updateSchedule(event.getId(), newSchedule, mode);
                    }
                });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void createSchedule(Schedule newSchedule, String mode) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.createSchedule("Bearer " + sessionManager.getUserToken(), newSchedule).enqueue(new Callback<Schedule>() {
            @Override
            public void onResponse(Call<Schedule> call, Response<Schedule> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), getContext().getText(R.string.add_schedule_success), Toast.LENGTH_LONG).show();
                    updateScheduleData(mode);
                } else {
                    handleError();
                }
            }

            @Override
            public void onFailure(Call<Schedule> call, Throwable t) {
                handleError();
            }
        });
    }

    private void handleDelete(Long scheduleId, String mode) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.deleteSchedule("Bearer " + sessionManager.getUserToken(), scheduleId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), getContext().getText(R.string.delete_schedule_successful), Toast.LENGTH_LONG).show();
                    updateScheduleData(mode);
                } else {
                    handleError();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                handleError();
            }
        });
    }

    private void updateScheduleData(String mode) {
        switch (mode) {
            case "admin":
                getSchedulesForAdmin();
                break;
            case "instructor":
                Long instructorId = getArguments().getLong("instructorId");
                getSchedulesForInstructor(instructorId);
                break;
            case "group":
                Long groupId = getArguments().getLong("groupId");
                getSchedulesForGroup(groupId);
                break;
            default:
                break;
        }
    }

    private void handleError() {
        Toast.makeText(getContext(), getContext().getText(R.string.error), Toast.LENGTH_LONG).show();
    }

    private void getSchedulesForAdmin() {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getSchedules("Bearer " + sessionManager.getUserToken()).enqueue(new Callback<List<Schedule>>() {
            @Override
            public void onResponse(Call<List<Schedule>> call, Response<List<Schedule>> response) {
                if (response.isSuccessful()) {
                    scheduleViewModel.setSchedules(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Schedule>> call, Throwable t) {

            }
        });
    }

    private void getSchedulesForInstructor(Long instructorId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getSchedulesByInstructor("Bearer " + sessionManager.getUserToken(), instructorId).enqueue(new Callback<List<Schedule>>() {
            @Override
            public void onResponse(Call<List<Schedule>> call, Response<List<Schedule>> response) {
                if (response.isSuccessful()) {
                    scheduleViewModel.setSchedules(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Schedule>> call, Throwable t) {

            }
        });
    }

    private void getSchedulesForGroup(Long groupId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getSchedulesByGroup("Bearer " + sessionManager.getUserToken(), groupId).enqueue(new Callback<List<Schedule>>() {
            @Override
            public void onResponse(Call<List<Schedule>> call, Response<List<Schedule>> response) {
                if (response.isSuccessful()) {
                    scheduleViewModel.setSchedules(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Schedule>> call, Throwable t) {
            }
        });
    }

    private void getGroups() {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getAllGroup().enqueue(new Callback<List<Group>>() {
            @Override
            public void onResponse(Call<List<Group>> call, Response<List<Group>> response) {
                if (response.isSuccessful()) {
                    scheduleViewModel.setGroups(response.body());
                }

            }

            @Override
            public void onFailure(Call<List<Group>> call, Throwable t) {

            }
        });
    }
}
