package pl.zmarzlak.danceschool.ui.changePassword;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.Role;
import pl.zmarzlak.danceschool.models.User;
import pl.zmarzlak.danceschool.service.APIService;
import pl.zmarzlak.danceschool.service.RetrofitClient;
import pl.zmarzlak.danceschool.service.SessionManager;
import pl.zmarzlak.danceschool.ui.editUser.EditUserViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordFragment extends Fragment {

    private ChangePasswordViewModel changePasswordViewModel;
    private SessionManager sessionManager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.change_password_fragment, container, false);
        Context context = root.getContext();
        sessionManager = new SessionManager(context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE));

        if (!sessionManager.isUserAuthorized()) {
            Navigation.findNavController(container).navigate(R.id.loginFragment);
        }

        Long userId = getArguments().getLong("userId");

        changePasswordViewModel = ViewModelProviders.of(this).get(ChangePasswordViewModel.class);
        final EditText newPassword = root.findViewById(R.id.new_password);
        final EditText newPasswordRepeat = root.findViewById(R.id.new_password_repeat);

        ((Button) root.findViewById(R.id.button_change_password)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = changePasswordViewModel.getUser().getValue();
                if (TextUtils.isEmpty(newPassword.getText()) || TextUtils.isEmpty(newPasswordRepeat.getText())
                        || !newPassword.getText().toString().equals(newPasswordRepeat.getText().toString())) {
                    Toast.makeText(getContext(), getContext().getString(R.string.bad_passwords), Toast.LENGTH_LONG).show();
                } else {
                    user.setPassword(newPassword.getText().toString());
                    handleEdit(root, userId, user);
                }
            }
        });

        getUserData(userId);
        return root;
    }

    private void handleEdit(View root, Long userId, User user) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.updateUserInfo("Bearer " + sessionManager.getUserToken(), userId, user).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), getContext().getString(R.string.change_password_success), Toast.LENGTH_LONG).show();
                    Navigation.findNavController(root).popBackStack();
                } else {
                    Toast.makeText(getContext(),getContext().getString(R.string.error), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getContext(),getContext().getString(R.string.error), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getUserData(Long userId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getUserInfo("Bearer " + sessionManager.getUserToken(), userId).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    changePasswordViewModel.setUser(response.body());
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }
}
