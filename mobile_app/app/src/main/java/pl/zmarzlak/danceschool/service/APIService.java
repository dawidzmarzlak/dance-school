package pl.zmarzlak.danceschool.service;

import java.util.List;

import pl.zmarzlak.danceschool.models.AuthRequest;
import pl.zmarzlak.danceschool.models.BlogPost;
import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.News;
import pl.zmarzlak.danceschool.models.Schedule;
import pl.zmarzlak.danceschool.models.TokenResponse;
import pl.zmarzlak.danceschool.models.User;
import pl.zmarzlak.danceschool.models.Vote;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIService {

    @GET("news")
    Call<List<News>> getNewsForAll();

    @GET("news/{id}")
    Call<News> getNews(@Header ("Authorization") String token, @Path("id") Long id);

    @GET("groups")
    Call<List<Group>> getAllGroup();

    @GET("instructor/groups/{id}")
    Call<List<Group>> getInstructorsGroups(@Header ("Authorization") String token, @Path("id") Long id);

    @POST("authenticate")
    Call<TokenResponse> authenticate(@Body AuthRequest authRequest);

    @GET("users/{id}")
    Call<User> getUserInfo(@Header ("Authorization") String token, @Path("id") Long id);

    @GET("users")
    Call<List<User>> getAllUsers(@Header ("Authorization") String token);

    @GET("students")
    Call<List<User>> getAllStudents(@Header ("Authorization") String token);

    @GET("students/{id}")
    Call<List<User>> getGroupStudents(@Header ("Authorization") String token, @Path("id") Long id);

    @GET("instructors")
    Call<List<User>> getAllInstructors(@Header ("Authorization") String token);

    @DELETE("users/{id}")
    Call<Void> deleteUser(@Header ("Authorization") String token, @Path("id") Long id);

    @DELETE("news/{id}")
    Call<Void> deleteNews(@Header ("Authorization") String token, @Path("id") Long id);

    @DELETE("groups/{id}")
    Call<Void> deleteGroup(@Header ("Authorization") String token, @Path("id") Long id);

    @POST("users/{id}")
    Call<User> updateUserInfo(@Header ("Authorization") String token, @Path("id") Long id, @Body User user);

    @POST("register")
    Call<User> register(@Body User user);

    @POST("news/{id}")
    Call<News> updateNews(@Header ("Authorization") String token, @Path("id") Long id, @Body News news);

    @POST("news")
    Call<News> addNews(@Header ("Authorization") String token, @Body News news);

    @GET("groups/{id}")
    Call<Group> getGroupInfo(@Header ("Authorization") String token, @Path("id") Long id);

    @POST("groups/{id}")
    Call<Group> updateGroupInfo(@Header ("Authorization") String token, @Path("id") Long id, @Body Group group);

    @POST("groups")
    Call<Group> addGroup(@Header ("Authorization") String token, @Body Group group);

    @GET("news/group/{id}")
    Call<List<News>> getNewsForGroup(@Header ("Authorization") String token, @Path("id") Long id);

    @GET("schedules")
    Call<List<Schedule>> getSchedules(@Header ("Authorization") String token);

    @GET("schedules/{id}")
    Call<List<Schedule>> getSchedulesByGroup(@Header ("Authorization") String token, @Path("id") Long id);

    @GET("instructor/schedules/{id}")
    Call<List<Schedule>> getSchedulesByInstructor(@Header ("Authorization") String token, @Path("id") Long id);

    @POST("schedules/{id}")
    Call<Schedule> updateSchedule(@Header ("Authorization") String token, @Path("id") Long id, @Body Schedule schedule);

    @DELETE("schedules/{id}")
    Call<Void> deleteSchedule(@Header ("Authorization") String token, @Path("id") Long id);

    @POST("schedules")
    Call<Schedule> createSchedule(@Header ("Authorization") String token, @Body Schedule schedule);

    @POST("vote")
    Call<Void> toggleVote(@Header ("Authorization") String token, @Body Vote vote);

    @HTTP(method = "DELETE", path = "blog/{id}", hasBody = true)
    Call<Void> deleteBlogPost(@Header ("Authorization") String token, @Path("id") Long id, @Body BlogPost blogPost);

    @POST("blog/{id}")
    Call<BlogPost> updateBlogPost(@Header ("Authorization") String token, @Path("id") Long id, @Body BlogPost blogPost);

    @POST("blog/group/{id}")
    Call<BlogPost> createBlogPost(@Header ("Authorization") String token, @Path("id") Long id, @Body BlogPost blogPost);

    @GET("blog/{id}")
    Call<BlogPost> getBlogPostById(@Header ("Authorization") String token, @Path("id") Long id);

    @GET("blog/group/{id}")
    Call<List<BlogPost>> getBlogPostByGroup(@Header ("Authorization") String token, @Path("id") Long id);
}
