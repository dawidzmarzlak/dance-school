package pl.zmarzlak.danceschool.service;

import android.content.SharedPreferences;
import android.net.wifi.hotspot2.pps.Credential;
import android.util.Log;
import android.widget.Toast;

import java.util.Objects;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import pl.zmarzlak.danceschool.models.AuthRequest;
import pl.zmarzlak.danceschool.models.TokenResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SessionManager {
    private final static String USER_TOKEN = "user_token";
    private final static String USER_LOGIN = "user_login";
    private final static String USER_PASSWORD = "user_password";
    private final static String USER_ID = "user_id";
    private static final byte[] keyValue = new byte[]{'c', 'o', 'd', 'i', 'n', 'g', 'a', 'q', 'w', 'f', 'h', 'j', 'x', 'a', 'h', 'n'};
    private SharedPreferences sharedPreferences;

    public SessionManager(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public boolean saveCredentials(String login, String password, String token, Long id) {
        try {
            return this.sharedPreferences.edit()
                    .putString(USER_LOGIN, login)
                    .putString(USER_PASSWORD, encryptPassword(password))
                    .putString(USER_TOKEN, token)
                    .putLong(USER_ID, id)
                    .commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public boolean deleteCredentials() {
        return this.sharedPreferences.edit().clear().commit();
    }

    public boolean isUserAuthorized() {
        String login = this.sharedPreferences.getString(USER_LOGIN, null);
        String password = this.sharedPreferences.getString(USER_PASSWORD, null);

        if (!Objects.isNull(login) && !Objects.isNull(password)) {
            try {
                String decryptedPassword = decryptPassword(password);
                tryRefreshToken(login, decryptedPassword);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return !Objects.isNull(getUserToken());
    }

    public String getUserToken() {
        return this.sharedPreferences.getString(USER_TOKEN, null);
    }

    public Long getUserId() {
        return this.sharedPreferences.getLong(USER_ID, -1);
    }

    private void tryRefreshToken(String login, String password) {
        Log.i("TEST", "refresh token");
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.authenticate(new AuthRequest(login, password)).enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                if (response.isSuccessful()) {
                    saveToken(response.body().getToken());
                } else {
                    deleteCredentials();
                }
            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t) {
                Log.i("ERR", "eror with connection");
            }
        });
    }

    private boolean saveToken(String token) {
        return this.sharedPreferences.edit().putString(USER_TOKEN, token).commit();
    }

    public static String encryptPassword(String cleartext) throws Exception {
        byte[] rawKey = getRawKey();
        byte[] result = encrypt(rawKey, cleartext.getBytes());
        return toHex(result);
    }

    public static String decryptPassword(String encrypted)
            throws Exception {

        byte[] enc = toByte(encrypted);
        byte[] result = decrypt(enc);
        return new String(result);
    }

    private static byte[] getRawKey() throws Exception {
        SecretKey key = new SecretKeySpec(keyValue, "AES");
        byte[] raw = key.getEncoded();
        return raw;
    }

    private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception {
        SecretKey skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(clear);
        return encrypted;
    }

    private static byte[] decrypt(byte[] encrypted) throws Exception {
        SecretKey skeySpec = new SecretKeySpec(keyValue, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decrypted = cipher.doFinal(encrypted);
        return decrypted;
    }

    public static byte[] toByte(String hexString) {
        int len = hexString.length() / 2;
        byte[] result = new byte[len];
        for (int i = 0; i < len; i++) {
            result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2),
                    16).byteValue();
        }

        return result;
    }

    public static String toHex(byte[] buf) {
        if (buf == null) {
            return "";
        }

        StringBuffer result = new StringBuffer(2 * buf.length);
        for (int i = 0; i < buf.length; i++) {
            appendHex(result, buf[i]);
        }
        return result.toString();
    }

    private final static String HEX = "0123456789ABCDEF";

    private static void appendHex(StringBuffer sb, byte b) {
        sb.append(HEX.charAt((b >> 4) & 0x0f)).append(HEX.charAt(b & 0x0f));
    }
}
