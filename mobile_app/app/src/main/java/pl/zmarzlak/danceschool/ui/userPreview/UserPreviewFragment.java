package pl.zmarzlak.danceschool.ui.userPreview;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.User;
import pl.zmarzlak.danceschool.service.APIService;
import pl.zmarzlak.danceschool.service.RetrofitClient;
import pl.zmarzlak.danceschool.service.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserPreviewFragment extends Fragment {

    private UserPreviewViewModel userPreviewViewModel;
    private SessionManager sessionManager;

    public static UserPreviewFragment newInstance() {
        return new UserPreviewFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.user_preview_fragment, container, false);
        Context context = root.getContext();
        sessionManager = new SessionManager(context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE));

        if (!sessionManager.isUserAuthorized()) {
            Navigation.findNavController(container).navigate(R.id.loginFragment);
        }

        Long userId = getArguments().getLong("userId");

        userPreviewViewModel = ViewModelProviders.of(this).get(UserPreviewViewModel.class);
        final TextView userName = root.findViewById(R.id.user_name_preview);
        final TextView userEmail = root.findViewById(R.id.user_email_preview);
        final TextView userGroup = root.findViewById(R.id.user_group_preview);

        ((Button) root.findViewById(R.id.button_edit)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putLong("userId", userId);
                Navigation.findNavController(root).navigate(R.id.editUserFragment, bundle);
            }
        });

        ((Button) root.findViewById(R.id.button_change_password)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putLong("userId", userId);
                Navigation.findNavController(root).navigate(R.id.changePasswordFragment, bundle);
            }
        });

        ((Button) root.findViewById(R.id.button_delete_account)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setCancelable(true);
                builder.setTitle(getContext().getText(R.string.delete_account));
                builder.setMessage(getContext().getText(R.string.delete_account_msg));
                builder.setPositiveButton(getContext().getText(R.string.confirm),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                handleDelete(root, userId);
                            }
                        });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        userPreviewViewModel.getUser().observe(getViewLifecycleOwner(), new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                if (!Objects.isNull(user)) {
                    Log.i("TAG", user.toString());
                    userName.setText(user.getFirstname() + " " + user.getLastname());
                    userEmail.setText(user.getEmail());
                    if (!Objects.isNull(user.getGroup())) {
                        userGroup.setText(user.getGroup().getName());
                    }
                }
            }
        });

        getUserData(userId);
        return root;
    }

    private void handleDelete(View root, Long userId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.deleteUser("Bearer " + sessionManager.getUserToken(), userId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    sessionManager.deleteCredentials();
                    Navigation.findNavController(root).navigate(R.id.loginFragment);
                    Toast.makeText(getContext(), getContext().getText(R.string.delete_user_successful), Toast.LENGTH_LONG).show();
                } else {
                    handleError();
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                handleError();
            }
        });
    }

    private void handleError() {
        Toast.makeText(getContext(), getContext().getText(R.string.error), Toast.LENGTH_LONG).show();

    }

    private void getUserData(Long userId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getUserInfo("Bearer " + sessionManager.getUserToken(), userId).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    userPreviewViewModel.setUser(response.body());
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }

}
