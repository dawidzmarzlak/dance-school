package pl.zmarzlak.danceschool.ui.profile;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import pl.zmarzlak.danceschool.MainActivity;
import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.ReminderBroadcast;
import pl.zmarzlak.danceschool.models.Schedule;
import pl.zmarzlak.danceschool.models.User;
import pl.zmarzlak.danceschool.service.APIService;
import pl.zmarzlak.danceschool.service.RetrofitClient;
import pl.zmarzlak.danceschool.service.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.ALARM_SERVICE;

public class ProfileFragment extends Fragment {

    private ProfileViewModel profileViewModel;
    private SessionManager sessionManager;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        Context context = root.getContext();
        sessionManager = new SessionManager(context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE));

        if (!sessionManager.isUserAuthorized()) {
            Navigation.findNavController(container).navigate(R.id.loginFragment);
        }

        profileViewModel =
                ViewModelProviders.of(this).get(ProfileViewModel.class);

        if (!Objects.isNull(profileViewModel.getUser().getValue())) {
            if ("student".equals(profileViewModel.getUser().getValue().getRole().getName())) {
                checkSchedule(profileViewModel.getUser().getValue().getGroup().getId());
            }
        }

        final TextView userName = root.findViewById(R.id.user_name);
        final TextView userEmail = root.findViewById(R.id.user_email);
        final TextView userGroup = root.findViewById(R.id.user_group);

        ((Button) root.findViewById(R.id.button_logout)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteNotifications(getSchedulesFromShared());
                if (sessionManager.deleteCredentials()) {
                    Navigation.findNavController(container).navigate(R.id.loginFragment);
                }
            }
        });
        profileViewModel.getUser().observe(getViewLifecycleOwner(), new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                if (!Objects.isNull(user)) {
                    Log.i("TAG", user.toString());
                    userName.setText(user.getFirstname() + " " + user.getLastname());
                    userEmail.setText(user.getEmail());
                    userGroup.setText(user.getGroup().getName());
                    configureButtons(root, user);
                    if ("student".equals(user.getRole().getName())) {
                        checkSchedule(user.getGroup().getId());
                    }
                }
            }
        });

        getUserData();
        return root;
    }

    private void configureButtons(View root, User user) {
        LinearLayout buttonsContainer = (LinearLayout) root.findViewById(R.id.profile_buttons_container);
        if (buttonsContainer.getChildCount() == 0) {
            switch (user.getRole().getName()) {
                case "admin":
                    buttonsContainer.addView(createScheduleButton(root, user, "admin"));
                    buttonsContainer.addView(createUsersListButton(root, user));
                    buttonsContainer.addView(createStudentsListButton(root, user, "admin"));
                    buttonsContainer.addView(createInstructorsListButton(root, user));
                    buttonsContainer.addView(createGroupsListButton(root, user, "all", R.id.allGroupsListFragment));
                    buttonsContainer.addView(createEditUserDataButton(root, user));
                    break;
                case "student":
                    buttonsContainer.addView(createNewsButton(root, user));
                    buttonsContainer.addView(createScheduleButton(root, user, "group"));
                    buttonsContainer.addView(createBlogButton(root, user));
                    buttonsContainer.addView(createEditUserDataButton(root, user));
                    break;
                case "instructor":
                    buttonsContainer.addView(createScheduleButton(root, user, "instructor"));
                    buttonsContainer.addView(createStudentsListButton(root, user, "instructor"));
                    buttonsContainer.addView(createGroupsListButton(root, user, "instructor", R.id.instructorGroupsListFragment));
                    buttonsContainer.addView(createEditUserDataButton(root, user));
                    break;
                default:
                    break;
            }
        }
    }

    private void getUserData() {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getUserInfo("Bearer " + sessionManager.getUserToken(), sessionManager.getUserId()).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    profileViewModel.setUser(response.body());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }

    private void checkSchedule(Long id) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getSchedulesByGroup("Bearer " + sessionManager.getUserToken(), id).enqueue(new Callback<List<Schedule>>() {
            @Override
            public void onResponse(Call<List<Schedule>> call, Response<List<Schedule>> response) {
                if (response.isSuccessful()) {
                    List<Schedule> schedulesFromShared = getSchedulesFromShared();
                    List<Schedule> toAdd = new LinkedList<>();
                    List<Schedule> toDelete = new LinkedList<>();
                    if (schedulesFromShared.size() > response.body().size()) {
                        schedulesFromShared.forEach(schedule -> {
                            int pos = response.body().indexOf(schedule);
                            if (pos != -1) {
                                Schedule prevSchedule = response.body().get(pos);
                                if (!schedule.getEnd().equals(prevSchedule.getEnd())
                                        || !schedule.getStart().equals(prevSchedule.getStart())
                                        || !schedule.getDate().equals(prevSchedule.getDate())) {
                                    toAdd.add(schedule);
                                    toDelete.add(prevSchedule);
                                }
                            } else {
                                toAdd.add(schedule);
                            }
                        });
                    } else {
                        response.body().forEach(schedule -> {
                            int pos = schedulesFromShared.indexOf(schedule);
                            if (pos != -1) {
                                Schedule prevSchedule = schedulesFromShared.get(pos);
                                if (!schedule.getEnd().equals(prevSchedule.getEnd())
                                        || !schedule.getStart().equals(prevSchedule.getStart())
                                        || !schedule.getDate().equals(prevSchedule.getDate())) {
                                    toAdd.add(schedule);
                                    toDelete.add(prevSchedule);
                                }
                            } else {
                                toAdd.add(schedule);
                            }
                            if (toAdd.size() > 0 || toDelete.size() > 0) {
                                Toast.makeText(getContext(), getContext().getString(R.string.schedule_diff), Toast.LENGTH_LONG).show();
                                deleteNotifications(toDelete);
                                setNotifications(toAdd);
                                setSchedulesToShared(response.body());
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Schedule>> call, Throwable t) {
            }
        });
    }

    private void setNotifications(List<Schedule> schedules) {
        schedules.forEach(schedule -> {
            PendingIntent pendingIntent = createPendingIntent(schedule);
            AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);
            long notificationTime = schedule.getStart().getTime() - (15 * 60 * 1000);
            if (notificationTime >= System.currentTimeMillis()) {
                alarmManager.set(AlarmManager.RTC_WAKEUP, notificationTime, pendingIntent);
            }
        });
    }

    private List<Schedule> getSchedulesFromShared() {
        List<Schedule> schedulesFromShared = new LinkedList<>();
        String jsonSchedules = sessionManager.getSharedPreferences().getString("schedule", "");
        Type type = new TypeToken<List<Schedule>>() {}.getType();
        Gson gson = new Gson();
        schedulesFromShared = gson.fromJson(jsonSchedules, type);
        if (Objects.isNull(schedulesFromShared)) {
            return new LinkedList<>();
        }
        return schedulesFromShared;
    }

    private void setSchedulesToShared(List<Schedule> schedules) {
        Gson gson = new Gson();
        String json = gson.toJson(schedules);
        sessionManager.getSharedPreferences().edit().putString("schedule", json).commit();
    }

    private void deleteNotifications(List<Schedule> schedules) {
        if (!Objects.isNull(schedules)) {
            schedules.forEach(schedule -> {
                PendingIntent pendingIntent = createPendingIntent(schedule);
                AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);
                alarmManager.cancel(pendingIntent);
            });
        }
    }

    private PendingIntent createPendingIntent(Schedule schedule) {
        Intent intent = new Intent(getContext(), ReminderBroadcast.class);
        String startDate = new SimpleDateFormat("HH:mm").format(schedule.getStart().getTime());
        String endDate = new SimpleDateFormat("HH:mm").format(schedule.getEnd().getTime());
        String content = getContext().getString(R.string.schedule_title) + schedule.getGroup().getName() +
                " " + startDate + " - " + endDate;
        intent.putExtra("content", content);
        intent.putExtra("id", schedule.getId().intValue());
        return PendingIntent.getBroadcast(getContext(), schedule.getId().intValue(), intent, 0);
    }

    private Button createNewsButton(View root, User user) {
        Button newsButton = createCustomButton();
        newsButton.setText(getContext().getString(R.string.news_button) + " " + user.getGroup().getName());
        newsButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putLong("groupId", user.getGroup().getId());
                bundle.putString("title", getContext().getText(R.string.news_for_group).toString() + " " + user.getGroup().getName());
                Navigation.findNavController(root).navigate(R.id.navigation_news_for_group, bundle);
            }
        });
        return newsButton;
    }

    private Button createBlogButton(View root, User user) {
        Button newsButton = createCustomButton();
        newsButton.setText(getContext().getString(R.string.blog_button) + " " + user.getGroup().getName());
        newsButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putLong("groupId", user.getGroup().getId());
                bundle.putLong("userId", user.getId());
                bundle.putString("title", getContext().getText(R.string.blog_button).toString() + " " + user.getGroup().getName());
                Navigation.findNavController(root).navigate(R.id.blogFragment, bundle);
            }
        });
        return newsButton;
    }

    private Button createScheduleButton(View root, User user, String mode) {
        Button scheduleButton = createCustomButton();
        scheduleButton.setText(getContext().getString(R.string.schedule_button));
        scheduleButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("mode", mode);
                if ("group".equals(mode)) {
                    bundle.putLong("groupId", user.getGroup().getId());
                } else if ("instructor".equals(mode)) {
                    bundle.putLong("instructorId", user.getId());
                    bundle.putString("userPriviliges", "edit");
                } else if ("admin".equals(mode)) {
                    bundle.putString("userPriviliges", "edit");
                }
                Navigation.findNavController(root).navigate(R.id.scheduleFragment, bundle);
            }
        });
        return scheduleButton;
    }

    private Button createUsersListButton(View root, User user) {
        Button usersListButton = createCustomButton();
        usersListButton.setText(getContext().getString(R.string.user_list));
        usersListButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("mode", "all");
                Navigation.findNavController(root).navigate(R.id.allUserListFragment, bundle);
            }
        });
        return usersListButton;
    }

    private Button createInstructorsListButton(View root, User user) {
        Button instructorsListButton = createCustomButton();
        instructorsListButton.setText(getContext().getString(R.string.instructors_list));
        instructorsListButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("mode", "instructors");
                Navigation.findNavController(root).navigate(R.id.allInstructorsListFragment, bundle);
            }
        });
        return instructorsListButton;
    }

    private Button createStudentsListButton(View root, User user, String mode) {
        Button studentsListButton = createCustomButton();
        studentsListButton.setText(getContext().getString(R.string.students_list));
        studentsListButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("mode", "students");
                Navigation.findNavController(root).navigate(R.id.allStudentsListFragment, bundle);
            }
        });
        return studentsListButton;
    }

    private Button createGroupsListButton(View root, User user, String mode, int navId) {
        Button groupsListButton = createCustomButton();
        groupsListButton.setText(getContext().getString(R.string.groups_list));
        groupsListButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("mode", mode);
                Navigation.findNavController(root).navigate(navId, bundle);
            }
        });
        return groupsListButton;
    }

    private Button createEditUserDataButton(View root, User user) {
        Button editUserDataButton = createCustomButton();
        editUserDataButton.setText(getContext().getString(R.string.edit_user_data_button));
        editUserDataButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putLong("userId", sessionManager.getUserId());
                Navigation.findNavController(root).navigate(R.id.userPreviewFragment, bundle);
            }
        });
        return editUserDataButton;
    }

    private Button createCustomButton() {
        Button button = new Button(getContext());
        button.setWidth(FrameLayout.LayoutParams.MATCH_PARENT);
        button.setHeight(250);
        return button;
    }
}
