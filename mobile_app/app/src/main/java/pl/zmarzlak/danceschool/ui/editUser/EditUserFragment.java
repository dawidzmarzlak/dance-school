package pl.zmarzlak.danceschool.ui.editUser;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.Role;
import pl.zmarzlak.danceschool.models.User;
import pl.zmarzlak.danceschool.service.APIService;
import pl.zmarzlak.danceschool.service.RetrofitClient;
import pl.zmarzlak.danceschool.service.SessionManager;
import pl.zmarzlak.danceschool.ui.userPreview.UserPreviewViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditUserFragment extends Fragment {

    private EditUserViewModel editUserViewModel;
    private SessionManager sessionManager;

    public static EditUserFragment newInstance() {
        return new EditUserFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.edit_user_fragment, container, false);
        Context context = root.getContext();
        sessionManager = new SessionManager(context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE));

        if (!sessionManager.isUserAuthorized()) {
            Navigation.findNavController(container).navigate(R.id.loginFragment);
        }

        Long userId = getArguments().getLong("userId");

        editUserViewModel = ViewModelProviders.of(this).get(EditUserViewModel.class);
        final EditText username = root.findViewById(R.id.user_username_edit);
        final EditText userEmail = root.findViewById(R.id.user_email_edit);
        final Spinner userGroup = root.findViewById(R.id.group_spinner);
        final Spinner userRole = root.findViewById(R.id.role_spinner);
        final EditText userFirstname = root.findViewById(R.id.user_firstname_edit);
        final EditText userLastname = root.findViewById(R.id.user_lastname_edit);

        ArrayAdapter<Role> roleArrayAdapter = new ArrayAdapter<Role>(getContext(), R.layout.support_simple_spinner_dropdown_item, new Role[] {new Role("admin"), new Role("student"), new Role("instructor")});
        userRole.setAdapter(roleArrayAdapter);

        ((Button) root.findViewById(R.id.button_save_edit)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(username.getText()) || TextUtils.isEmpty(userEmail.getText())
                        || TextUtils.isEmpty(userFirstname.getText()) || TextUtils.isEmpty(userLastname.getText()) || !userEmail.getText().toString().contains("@")) {
                    Toast.makeText(getContext(), getContext().getString(R.string.fill_empty_fields), Toast.LENGTH_LONG).show();
                } else {
                    User user = new User(username.getText().toString(), userEmail.getText().toString(), "",
                            (Role) userRole.getSelectedItem(), (Group) userGroup.getSelectedItem(), userFirstname.getText().toString(), userLastname.getText().toString());
                    handleEdit(root, userId, user);
                }

            }
        });

        editUserViewModel.getUser().observe(getViewLifecycleOwner(), new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                if (!Objects.isNull(user)) {
                    Log.i("TAG", user.toString());
                    username.setText(user.getUsername());
                    userFirstname.setText(user.getFirstname());
                    userLastname.setText(user.getLastname());
                    userEmail.setText(user.getEmail());
                    int selectGroupPosition = editUserViewModel.getGroups().getValue().indexOf(user.getGroup());
                    userGroup.setSelection(selectGroupPosition);
                    userRole.setSelection(user.getRole().getId().intValue() - 1);
                }
            }
        });

        editUserViewModel.getAdminView().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                userRole.setEnabled(aBoolean);
            }
        });

        editUserViewModel.getGroups().observe(getViewLifecycleOwner(), new Observer<List<Group>>() {
            @Override
            public void onChanged(@Nullable List<Group> groups) {
                Group [] groupsArray = new Group[groups.size()];
                groups.toArray(groupsArray);
                ArrayAdapter<Group> groupArrayAdapter = new ArrayAdapter<Group>(getContext(), R.layout.support_simple_spinner_dropdown_item, groupsArray);
                userGroup.setAdapter(groupArrayAdapter);
            }
        });

        getUserData(userId);
        getUserData();
        getGroups();
        return root;
    }

    private void handleEdit(View root, Long userId, User user) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.updateUserInfo("Bearer " + sessionManager.getUserToken(), userId, user).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), getContext().getString(R.string.edit_success), Toast.LENGTH_LONG).show();
                    Navigation.findNavController(root).popBackStack();
                } else {
                    Toast.makeText(getContext(),getContext().getString(R.string.edit_bad_data), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getContext(),getContext().getString(R.string.error), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getUserData(Long userId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getUserInfo("Bearer " + sessionManager.getUserToken(), userId).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    editUserViewModel.setUser(response.body());
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }

    private void getUserData() {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getUserInfo("Bearer " + sessionManager.getUserToken(), sessionManager.getUserId()).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    editUserViewModel.setAdminView("admin".equals(response.body().getRole().getName()));
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }

    private void getGroups() {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getAllGroup().enqueue(new Callback<List<Group>>() {
            @Override
            public void onResponse(Call<List<Group>> call, Response<List<Group>> response) {
                if (response.isSuccessful()) {
                    editUserViewModel.setGroups(response.body());
                }

            }

            @Override
            public void onFailure(Call<List<Group>> call, Throwable t) {

            }
        });
    }
}
