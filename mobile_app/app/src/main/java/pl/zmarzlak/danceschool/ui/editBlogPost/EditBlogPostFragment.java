package pl.zmarzlak.danceschool.ui.editBlogPost;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Objects;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.BlogPost;
import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.News;
import pl.zmarzlak.danceschool.models.User;
import pl.zmarzlak.danceschool.service.APIService;
import pl.zmarzlak.danceschool.service.RetrofitClient;
import pl.zmarzlak.danceschool.service.SessionManager;
import pl.zmarzlak.danceschool.ui.editNews.EditNewsViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class EditBlogPostFragment extends Fragment {

    private EditBlogPostViewModel editBlogPostViewModel;
    private SessionManager sessionManager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.edit_blog_post_fragment, container, false);
        Context context = root.getContext();
        sessionManager = new SessionManager(context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE));

        if (!sessionManager.isUserAuthorized()) {
            Navigation.findNavController(container).navigate(R.id.loginFragment);
        }

        String mode = getArguments().getString("mode");
        Long groupId = getArguments().getLong("groupId");
        Long tmpPostId = -1L;

        if ("edit".equals(mode)) {
            tmpPostId = getArguments().getLong("postId");
        }

        Long postId = tmpPostId;

        if ("edit".equals(mode)) {
            getBlogPostData(postId);
            LinearLayout linearLayout = root.findViewById(R.id.edit_blog_post_linear_layout);
            Button deleteButton = new Button(getContext());
            deleteButton.setText(getContext().getString(R.string.delete_blog_post));
            deleteButton.setBackgroundColor(getContext().getColor(R.color.delete_button));
            deleteButton.setTextColor(Color.WHITE);
            deleteButton.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            deleteButton.setOnClickListener(new Button.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setCancelable(true);
                    builder.setTitle(getContext().getText(R.string.delete_blog_post));
                    builder.setMessage(getContext().getText(R.string.delete_blog_post_msg));
                    builder.setPositiveButton(getContext().getText(R.string.confirm),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handleDelete(root, postId, editBlogPostViewModel.getBlogPost().getValue());
                                }
                            });
                    builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            });
            linearLayout.addView(deleteButton);
        }

        editBlogPostViewModel = ViewModelProviders.of(this).get(EditBlogPostViewModel.class);
        final EditText content = root.findViewById(R.id.blog_post_content_edit);
        final ImageView image = root.findViewById(R.id.add_post_image_preview);

        ((Button) root.findViewById(R.id.blog_save_button)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Group group = new Group();
                group.setId(groupId);
                User user = new User();
                user.setId(sessionManager.getUserId());
                BlogPost blogPost = new BlogPost(content.getText().toString(),
                        editBlogPostViewModel.getImage().getValue(), group, user);

                switch (mode) {
                    case "add":
                        handleAdd(root, blogPost, groupId);
                        break;
                    case "edit":
                        handleEdit(root, postId, blogPost);
                        break;
                    default:
                        break;
                }
            }
        });

        ((Button) root.findViewById(R.id.choose_image_button)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cameraIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(cameraIntent, 1000);
            }
        });

        editBlogPostViewModel.getBlogPost().observe(getViewLifecycleOwner(), new Observer<BlogPost>() {
            @Override
            public void onChanged(@Nullable BlogPost blogPost) {
                if (!Objects.isNull(blogPost) && "edit".equals(mode)) {
                    content.setText(blogPost.getContent());
                }
            }
        });

        editBlogPostViewModel.getImage().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (!s.isEmpty()) {
                    image.setImageBitmap(convertToBitmap(s));
                }
            }
        });

        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 1000){
                try {
                    Uri returnUri = data.getData();
                    Bitmap bitmapImage = null;
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmapImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), returnUri);
                    bitmapImage.compress(Bitmap.CompressFormat.JPEG, 70, baos);
                    byte[] imageBytes = baos.toByteArray();
                    String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                    editBlogPostViewModel.setImage(imageString);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private Bitmap convertToBitmap(String imageString) {
        byte[] imageBytes = Base64.decode(imageString, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
    }

    private void handleEdit(View root, Long postId, BlogPost blogPost) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.updateBlogPost("Bearer " + sessionManager.getUserToken(), postId, blogPost).enqueue(new Callback<BlogPost>() {
            @Override
            public void onResponse(Call<BlogPost> call, Response<BlogPost> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), getContext().getString(R.string.edit_success), Toast.LENGTH_LONG).show();
                    Navigation.findNavController(root).popBackStack();
                }
            }

            @Override
            public void onFailure(Call<BlogPost> call, Throwable t) {
                Toast.makeText(getContext(),getContext().getString(R.string.error), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void handleAdd(View root, BlogPost blogPost, Long groupId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.createBlogPost("Bearer " + sessionManager.getUserToken(), groupId, blogPost).enqueue(new Callback<BlogPost>() {
            @Override
            public void onResponse(Call<BlogPost> call, Response<BlogPost> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), getContext().getString(R.string.edit_success), Toast.LENGTH_LONG).show();
                    Navigation.findNavController(root).popBackStack();
                }
            }

            @Override
            public void onFailure(Call<BlogPost> call, Throwable t) {
                Toast.makeText(getContext(),getContext().getString(R.string.error), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void handleDelete(View root, Long postId, BlogPost blogPost) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.deleteBlogPost("Bearer " + sessionManager.getUserToken(), postId, blogPost).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Navigation.findNavController(root).popBackStack();
                    Toast.makeText(getContext(), getContext().getText(R.string.delete_post_successful), Toast.LENGTH_LONG).show();
                } else {
                    handleError();
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                handleError();
            }
        });
    }

    private void handleError() {
        Toast.makeText(getContext(), getContext().getText(R.string.error), Toast.LENGTH_LONG).show();
    }

    private void getBlogPostData(Long postId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getBlogPostById("Bearer " + sessionManager.getUserToken(), postId).enqueue(new Callback<BlogPost>() {
            @Override
            public void onResponse(Call<BlogPost> call, Response<BlogPost> response) {
                if (response.isSuccessful()) {
                    editBlogPostViewModel.setBlogPost(response.body());
                    editBlogPostViewModel.setImage(response.body().getImage());
                }
            }

            @Override
            public void onFailure(Call<BlogPost> call, Throwable t) {

            }
        });
    }

}
