package pl.zmarzlak.danceschool.ui.blog;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.List;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.BlogPost;
import pl.zmarzlak.danceschool.models.User;
import pl.zmarzlak.danceschool.models.Vote;
import pl.zmarzlak.danceschool.service.APIService;
import pl.zmarzlak.danceschool.service.RetrofitClient;
import pl.zmarzlak.danceschool.service.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BlogViewModel extends RecyclerView.Adapter<BlogViewModel.ViewHolder> {

    private List<BlogPost> blogPostList;
    private View root;
    private Long groupId;
    private Long userId;
    private boolean isAdminView;
    private SessionManager sessionManager;

    public void setBlogPostList(List<BlogPost> blogPostList) {
        this.blogPostList = blogPostList;
    }

    public void setAdminView(boolean adminView) {
        isAdminView = adminView;
    }

    public BlogViewModel(List<BlogPost> blogPostList, View root, Long groupId, Long userId,
                         boolean isAdminView, SessionManager sessionManager) {
        this.blogPostList = blogPostList;
        this.root = root;
        this.groupId = groupId;
        this.userId = userId;
        this.isAdminView = isAdminView;
        this.sessionManager = sessionManager;
    }

    @NonNull
    @Override
    public BlogViewModel.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View blogListElem = inflater.inflate(R.layout.blog_list_elem, parent, false);
        ViewHolder viewHolder = new ViewHolder(blogListElem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BlogPost blogPost = blogPostList.get(position);

        if (blogPost.getUser().getId() == userId || this.isAdminView) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putString("mode", "edit");
                    bundle.putLong("postId", blogPost.getId());
                    bundle.putLong("groupId", groupId);
                    Navigation.findNavController(root).navigate(R.id.editBlogPostFragment, bundle);
                }
            });
        }

        TextView title = holder.title;
        String author = blogPost.getUser().getFirstname() + " " + blogPost.getUser().getLastname();
        title.setText(author);

        TextView date = holder.date;
        date.setText(new SimpleDateFormat("yyyy-MM-dd").format(blogPost.getDate()));

        ImageView image = holder.image;
        image.setImageBitmap(convertToBitmap(blogPost.getImage()));

        TextView content = holder.content;
        content.setText(blogPost.getContent());

        TextView votes = holder.votes;
        votes.setText(root.getContext().getString(R.string.votes) + " " + blogPost.getVotes().size());

        ImageButton voteButton = holder.voteButton;
        Vote vote = new Vote(userId, blogPost.getId());
        voteButton.setImageDrawable(root.getContext().getDrawable(blogPost.getVotes().indexOf(vote) != -1
                ? R.drawable.ic_favorite_black_24dp : R.drawable.ic_favorite_border_black_24dp));
        voteButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleVote(vote);
            }
        });
    }

    @Override
    public int getItemCount() {
        return blogPostList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView date;
        private TextView content;
        private ImageView image;
        private ImageButton voteButton;
        private TextView votes;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.blog_author);
            date = (TextView) itemView.findViewById(R.id.blog_date);
            content = (TextView) itemView.findViewById(R.id.blog_content);
            votes = (TextView) itemView.findViewById(R.id.blog_votes);
            voteButton = (ImageButton) itemView.findViewById(R.id.blog_vote_button);
            image = (ImageView) itemView.findViewById(R.id.blog_image);
        }
    }

    private void toggleVote(Vote vote) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.toggleVote("Bearer " + sessionManager.getUserToken(), vote).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    blogPostList.stream().forEach(blogPost -> {
                        if (blogPost.getId() == vote.getBlogPost()) {
                            int index = blogPost.getVotes().indexOf(vote);
                            if (index != -1) {
                                blogPost.getVotes().remove(index);
                            } else {
                                blogPost.getVotes().add(vote);
                            }
                        }
                    });
                    notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.i("ERR", "error vote");
            }
        });
    }

    private Bitmap convertToBitmap(String imageString) {
        byte[] imageBytes = Base64.decode(imageString, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
    }
}
