package pl.zmarzlak.danceschool.ui.editNews;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.News;
import pl.zmarzlak.danceschool.models.Role;
import pl.zmarzlak.danceschool.models.User;
import pl.zmarzlak.danceschool.service.APIService;
import pl.zmarzlak.danceschool.service.RetrofitClient;
import pl.zmarzlak.danceschool.service.SessionManager;
import pl.zmarzlak.danceschool.ui.editUser.EditUserViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditNewsFragment extends Fragment {

    private EditNewsViewModel editNewsViewModel;
    private SessionManager sessionManager;

    public static EditNewsFragment newInstance() {
        return new EditNewsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.edit_news_fragment, container, false);
        Context context = root.getContext();
        sessionManager = new SessionManager(context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE));

        if (!sessionManager.isUserAuthorized()) {
            Navigation.findNavController(container).navigate(R.id.loginFragment);
        }

        String mode = getArguments().getString("mode");
        Long groupId = getArguments().getLong("groupId");
        Long tmpNewsId = -1L;

        if ("edit".equals(mode)) {
            tmpNewsId = getArguments().getLong("newsId");
        }

        Long newsId = tmpNewsId;

        if ("edit".equals(mode)) {
            getNewsData(newsId);
            LinearLayout linearLayout = root.findViewById(R.id.edit_news_linear_layout);
            Button deleteButton = new Button(getContext());
            deleteButton.setText(getContext().getString(R.string.delete_news));
            deleteButton.setBackgroundColor(getContext().getColor(R.color.delete_button));
            deleteButton.setTextColor(Color.WHITE);
            deleteButton.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            deleteButton.setOnClickListener(new Button.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setCancelable(true);
                    builder.setTitle(getContext().getText(R.string.delete_news));
                    builder.setMessage(getContext().getText(R.string.delete_news_msg));
                    builder.setPositiveButton(getContext().getText(R.string.confirm),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    handleDelete(root, newsId);
                                }
                            });
                    builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            });
            linearLayout.addView(deleteButton);
        }

        editNewsViewModel = ViewModelProviders.of(this).get(EditNewsViewModel.class);
        final EditText title = root.findViewById(R.id.news_title_edit);
        final EditText content = root.findViewById(R.id.news_content_edit);

        ((Button) root.findViewById(R.id.news_save_button)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(title.getText()) || TextUtils.isEmpty(content.getText())) {
                    Toast.makeText(getContext(), getContext().getString(R.string.fill_empty_fields), Toast.LENGTH_LONG).show();
                } else {
                    News news = new News(title.getText().toString(), content.getText().toString());
                    if (groupId > -1L) {
                        Group group = new Group();
                        group.setId(groupId);
                        news.setGroup(group);
                    }
                    switch (mode) {
                        case "add":
                            handleAdd(root, news);
                            break;
                        case "edit":
                            handleEdit(root, newsId, news);
                            break;
                        default:
                            break;
                    }
                }

            }
        });

        editNewsViewModel.getNews().observe(getViewLifecycleOwner(), new Observer<News>() {
            @Override
            public void onChanged(@Nullable News news) {
                if (!Objects.isNull(news) && "edit".equals(mode)) {
                    title.setText(news.getTitle());
                    content.setText(news.getContent());
                }
            }
        });

        return root;
    }

    private void handleEdit(View root, Long newsId, News news) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.updateNews("Bearer " + sessionManager.getUserToken(), newsId, news).enqueue(new Callback<News>() {
            @Override
            public void onResponse(Call<News> call, Response<News> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), getContext().getString(R.string.edit_success), Toast.LENGTH_LONG).show();
                    Navigation.findNavController(root).popBackStack();
                } else {
                    Toast.makeText(getContext(),getContext().getString(R.string.edit_bad_data), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<News> call, Throwable t) {
                Toast.makeText(getContext(),getContext().getString(R.string.error), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void handleAdd(View root, News news) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.addNews("Bearer " + sessionManager.getUserToken(), news).enqueue(new Callback<News>() {
            @Override
            public void onResponse(Call<News> call, Response<News> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), getContext().getString(R.string.edit_success), Toast.LENGTH_LONG).show();
                    Navigation.findNavController(root).popBackStack();
                } else {
                    Toast.makeText(getContext(), getContext().getString(R.string.news_bad_data), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<News> call, Throwable t) {
                Toast.makeText(getContext(),getContext().getString(R.string.error), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void handleDelete(View root, Long newsId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.deleteNews("Bearer " + sessionManager.getUserToken(), newsId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Navigation.findNavController(root).popBackStack();
                    Toast.makeText(getContext(), getContext().getText(R.string.delete_news_successful), Toast.LENGTH_LONG).show();
                } else {
                    handleError();
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                handleError();
            }
        });
    }

    private void handleError() {
        Toast.makeText(getContext(), getContext().getText(R.string.error), Toast.LENGTH_LONG).show();
    }

    private void getNewsData(Long newsId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getNews("Bearer " + sessionManager.getUserToken(), newsId).enqueue(new Callback<News>() {
            @Override
            public void onResponse(Call<News> call, Response<News> response) {
                if (response.isSuccessful()) {
                    editNewsViewModel.setNews(response.body());
                }

            }

            @Override
            public void onFailure(Call<News> call, Throwable t) {

            }
        });
    }

}
