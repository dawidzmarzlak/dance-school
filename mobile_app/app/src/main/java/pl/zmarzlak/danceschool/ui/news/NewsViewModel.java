package pl.zmarzlak.danceschool.ui.news;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.view.NestedScrollingChild;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.News;

public class NewsViewModel extends RecyclerView.Adapter<NewsViewModel.ViewHolder> {

    private List<News> newsList;
    private boolean isAdminView;
    private View root;
    private Long groupId;

    public void setAdminView(boolean adminView) {
        isAdminView = adminView;
    }

    public void setNewsList(List<News> newsList) {
        this.newsList = newsList;
    }

    public NewsViewModel(List<News> newsList, View root, Long groupId) {
        this.newsList = newsList;
        this.isAdminView = false;
        this.root = root;
        this.groupId = groupId;
    }

    public NewsViewModel(List<News> newsList, boolean isAdminView) {
        this.newsList = newsList;
        this.isAdminView = isAdminView;
    }

    @NonNull
    @Override
    public NewsViewModel.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View newsListElem = inflater.inflate(R.layout.news_list_element, parent, false);
        ViewHolder viewHolder = new ViewHolder(newsListElem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewModel.ViewHolder holder, int position) {
        News news = newsList.get(position);

        if (this.isAdminView) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putString("mode", "edit");
                    bundle.putLong("newsId", news.getId());
                    bundle.putLong("groupId", groupId);
                    Navigation.findNavController(root).navigate(R.id.editNewsFragment, bundle);
                }
            });
        }


        TextView title = holder.title;
        title.setText(news.getTitle());

        TextView date = holder.date;
        date.setText(new SimpleDateFormat("yyyy-MM-dd").format(news.getDate()));

        TextView content = holder.content;
        content.setText(news.getContent());
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView date;
        private TextView content;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.news_title);
            date = (TextView) itemView.findViewById(R.id.news_date);
            content = (TextView) itemView.findViewById(R.id.news_content);
        }
    }

}