package pl.zmarzlak.danceschool.ui.blog;

import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.BlogPost;
import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.News;
import pl.zmarzlak.danceschool.models.User;
import pl.zmarzlak.danceschool.service.APIService;
import pl.zmarzlak.danceschool.service.RetrofitClient;
import pl.zmarzlak.danceschool.service.SessionManager;
import pl.zmarzlak.danceschool.ui.news.NewsViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BlogFragment extends Fragment {

    private BlogViewModel blogViewModel;
    private List<BlogPost> blogPostList = new LinkedList<>();
    private SessionManager sessionManager;

    public static BlogFragment newInstance() {
        return new BlogFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.blog_fragment, container, false);
        Context context = root.getContext();
        sessionManager = new SessionManager(context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE));
        Bundle bundle = getArguments();
        if (!sessionManager.isUserAuthorized()) {
            Navigation.findNavController(root).navigate(R.id.loginFragment);
        }
        Long groupId = bundle.getLong("groupId");
        Long userId = bundle.getLong("userId");
        loadBlogPostsForGroupFromApi(groupId);
        loadUserData(userId);
        blogViewModel = new BlogViewModel(blogPostList, root, groupId, userId, false, sessionManager);
        RecyclerView recyclerView = root.findViewById(R.id.blog_list_recycler_view);
        recyclerView.setAdapter(blogViewModel);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));

        Button addPostButton = (Button) root.findViewById(R.id.blog_add_post_button);
        addPostButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle navBundle = new Bundle();
                navBundle.putString("mode", "add");
                navBundle.putLong("groupId", groupId);
                Navigation.findNavController(root).navigate(R.id.addBlogPostFragment, navBundle);
            }
        });

        return root;
    }

    private void loadBlogPostsForGroupFromApi(Long groupId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getBlogPostByGroup("Bearer " + sessionManager.getUserToken(), groupId).enqueue(new Callback<List<BlogPost>>() {
            @Override
            public void onResponse(Call<List<BlogPost>> call, Response<List<BlogPost>> response) {
                if (response.isSuccessful()) {
                    blogViewModel.setBlogPostList(response.body());
                    blogViewModel.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<BlogPost>> call, Throwable t) {
                Log.e("tag", "error API");
            }
        });
    }

    private void loadUserData(Long userId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getUserInfo("Bearer " + sessionManager.getUserToken(), userId).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    boolean isInstructorOfGroup = "instructor".equals(response.body().getRole().getName());
                    boolean isAdminView = "admin".equals(response.body().getRole().getName()) || isInstructorOfGroup;
                    blogViewModel.setAdminView(isAdminView);
                    blogViewModel.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }
}
