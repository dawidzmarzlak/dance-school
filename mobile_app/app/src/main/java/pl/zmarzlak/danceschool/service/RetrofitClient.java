package pl.zmarzlak.danceschool.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

import pl.zmarzlak.danceschool.models.DateTypeDeserializer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    public static Retrofit retrofit;

    public static Retrofit getClient() {
        if (retrofit == null) {
            Gson gsonBuilder = new GsonBuilder()
                    .registerTypeAdapter(Date.class, new DateTypeDeserializer())
                    .create();
            retrofit = new Retrofit.Builder()
                    .baseUrl("http://192.168.0.52/api/")
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder))
                    .build();
        }
        return retrofit;
    }
}
