package pl.zmarzlak.danceschool.ui.editGroup;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.LinkedList;
import java.util.List;

import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.User;

public class EditGroupViewModel extends ViewModel {
    private MutableLiveData<Group> mGroup;
    private MutableLiveData<List<User>> mInstructors;

    public EditGroupViewModel() {
        mGroup = new MutableLiveData<>();
        mGroup.setValue(null);
        mInstructors = new MutableLiveData<>();
        mInstructors.setValue(new LinkedList<>());
    }

    public LiveData<Group> getGroup() {
        return mGroup;
    }

    public void setGroup(Group group) {
        mGroup.postValue(group);
    }

    public LiveData<List<User>> getInstructors() {
        return mInstructors;
    }

    public void setInstructors(List<User> instructors) {
        mInstructors.postValue(instructors);
    }
}