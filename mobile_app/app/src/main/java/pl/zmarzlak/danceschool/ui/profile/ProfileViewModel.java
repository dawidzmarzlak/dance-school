package pl.zmarzlak.danceschool.ui.profile;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import pl.zmarzlak.danceschool.models.User;

public class ProfileViewModel extends ViewModel {

    private MutableLiveData<User> mUser;

    public ProfileViewModel() {
        mUser = new MutableLiveData<>();
        mUser.setValue(null);
    }

    public LiveData<User> getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser.postValue(user);
    }
}