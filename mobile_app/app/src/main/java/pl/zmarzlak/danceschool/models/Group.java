package pl.zmarzlak.danceschool.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Group {
    @SerializedName("id")
    @Expose
    private Long id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("instructorId")
    @Expose
    private Long instructorId;

    public Group() {
    }

    public Group(Long id, String name, Long instructorId) {
        this.id = id;
        this.name = name;
        this.instructorId = instructorId;
    }

    public Group(String name, Long instructorId) {
        this.name = name;
        this.instructorId = instructorId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getInstructorId() {
        return instructorId;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return id.equals(group.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void setInstructorId(Long instructorId) {
        this.instructorId = instructorId;
    }
}
