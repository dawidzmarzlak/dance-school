package pl.zmarzlak.danceschool.ui.editGroup;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.Role;
import pl.zmarzlak.danceschool.models.User;
import pl.zmarzlak.danceschool.service.APIService;
import pl.zmarzlak.danceschool.service.RetrofitClient;
import pl.zmarzlak.danceschool.service.SessionManager;
import pl.zmarzlak.danceschool.ui.editUser.EditUserViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditGroupFragment extends Fragment {

    private EditGroupViewModel editGroupViewModel;
    private SessionManager sessionManager;

    public static EditGroupFragment newInstance() {
        return new EditGroupFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.edit_group_fragment, container, false);
        Context context = root.getContext();
        sessionManager = new SessionManager(context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE));

        if (!sessionManager.isUserAuthorized()) {
            Navigation.findNavController(container).navigate(R.id.loginFragment);
        }

        String mode = getArguments().getString("mode");

        editGroupViewModel = ViewModelProviders.of(this).get(EditGroupViewModel.class);
        final EditText groupName = root.findViewById(R.id.group_name_edit);
        final Spinner instructorsSpinner = root.findViewById(R.id.instructor_spinner);

        ((Button) root.findViewById(R.id.button_save_edit_group)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(groupName.getText())) {
                    Toast.makeText(getContext(), getContext().getString(R.string.fill_empty_fields), Toast.LENGTH_LONG).show();
                } else {
                    Group group = new Group(groupName.getText().toString(), ((User) instructorsSpinner.getSelectedItem()).getId());
                    if ("edit".equals(mode)) {
                        Long groupId = getArguments().getLong("groupId");
                        handleEdit(root, groupId, group);
                    } else if ("add".equals(mode)) {
                        handleAdd(root, group);
                    }
                }
            }
        });

        editGroupViewModel.getGroup().observe(getViewLifecycleOwner(), new Observer<Group>() {
            @Override
            public void onChanged(@Nullable Group group) {
                if (!Objects.isNull(group) && "edit".equals(mode)) {
                    groupName.setText(group.getName());
                    int selectInstructorPosition = IntStream.range(0, editGroupViewModel.getInstructors().getValue().size())
                            .filter(i -> editGroupViewModel.getInstructors().getValue().get(i).getId().equals(group.getInstructorId()))
                            .findFirst()
                            .orElse(-1);
                    instructorsSpinner.setSelection(selectInstructorPosition);
                }
            }
        });

        editGroupViewModel.getInstructors().observe(getViewLifecycleOwner(), new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable List<User> instructors) {
                User [] instructorsArray = new User[instructors.size()];
                instructors.toArray(instructorsArray);
                ArrayAdapter<User> instructorsArrayAdapter = new ArrayAdapter<User>(getContext(), R.layout.support_simple_spinner_dropdown_item, instructorsArray);
                instructorsSpinner.setAdapter(instructorsArrayAdapter);
            }
        });

        if ("edit".equals(mode)) {
            Long groupId = getArguments().getLong("groupId");
            getGroupData(groupId);
        }

        getInstructors();
        return root;
    }

    private void handleEdit(View root, Long groupId, Group group) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.updateGroupInfo("Bearer " + sessionManager.getUserToken(), groupId, group).enqueue(new Callback<Group>() {
            @Override
            public void onResponse(Call<Group> call, Response<Group> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), getContext().getString(R.string.edit_success), Toast.LENGTH_LONG).show();
                    Navigation.findNavController(root).popBackStack();
                } else {
                    Toast.makeText(getContext(),getContext().getString(R.string.edit_bad_data), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Group> call, Throwable t) {
                Toast.makeText(getContext(),getContext().getString(R.string.error), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void handleAdd(View root, Group group) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.addGroup("Bearer " + sessionManager.getUserToken(), group).enqueue(new Callback<Group>() {
            @Override
            public void onResponse(Call<Group> call, Response<Group> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), getContext().getString(R.string.add_group_success), Toast.LENGTH_LONG).show();
                    Navigation.findNavController(root).popBackStack();
                } else {
                    Toast.makeText(getContext(),getContext().getString(R.string.edit_bad_data), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Group> call, Throwable t) {
                Toast.makeText(getContext(),getContext().getString(R.string.error), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getGroupData(Long groupId) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getGroupInfo("Bearer " + sessionManager.getUserToken(), groupId).enqueue(new Callback<Group>() {
            @Override
            public void onResponse(Call<Group> call, Response<Group> response) {
                if (response.isSuccessful()) {
                    editGroupViewModel.setGroup(response.body());
                }

            }

            @Override
            public void onFailure(Call<Group> call, Throwable t) {

            }
        });
    }

    private void getInstructors() {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getAllInstructors("Bearer " + sessionManager.getUserToken()).enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.isSuccessful()) {
                    editGroupViewModel.setInstructors(response.body());
                }

            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {

            }
        });
    }
}
