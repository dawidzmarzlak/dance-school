package pl.zmarzlak.danceschool.ui.editUser;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.LinkedList;
import java.util.List;

import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.User;

public class EditUserViewModel extends ViewModel {
    private MutableLiveData<User> mUser;
    private MutableLiveData<Boolean> mAdminView;
    private MutableLiveData<List<Group>> mGroups;

    public EditUserViewModel() {
        mUser = new MutableLiveData<>();
        mUser.setValue(null);
        mGroups = new MutableLiveData<>();
        mGroups.setValue(new LinkedList<>());
        mAdminView = new MutableLiveData<>();
        mAdminView.setValue(false);
    }

    public LiveData<User> getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser.postValue(user);
    }

    public LiveData<List<Group>> getGroups() {
        return mGroups;
    }

    public void setGroups(List<Group> groups) {
        mGroups.postValue(groups);
    }

    public LiveData<Boolean> getAdminView() {
        return mAdminView;
    }

    public void setAdminView(Boolean adminView) {
        mAdminView.postValue(adminView);
    }
}
