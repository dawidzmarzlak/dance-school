package pl.zmarzlak.danceschool.ui.groupsList;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.News;
import pl.zmarzlak.danceschool.models.User;
import pl.zmarzlak.danceschool.service.APIService;
import pl.zmarzlak.danceschool.service.RetrofitClient;
import pl.zmarzlak.danceschool.service.SessionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupsListFragment extends Fragment {

    private GroupsListViewModel groupsViewModel;
    private List<Group> groupsList = new LinkedList<>();
    private SessionManager sessionManager;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.groups_list_fragment, container, false);
        Context context = root.getContext();
        sessionManager = new SessionManager(context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE));
        Bundle bundle = getArguments();
        if (!sessionManager.isUserAuthorized()) {
            Navigation.findNavController(root).navigate(R.id.loginFragment);
        }
        String mode = bundle.getString("mode");
        loadGroupsFromApi(mode);

        groupsViewModel = new GroupsListViewModel(root, groupsList);
        RecyclerView recyclerView = root.findViewById(R.id.groups_list_recycler_view);
        recyclerView.setAdapter(groupsViewModel);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));

        final Button addGroup = root.findViewById(R.id.button_add_group);

        addGroup.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle newBundle = new Bundle();
                newBundle.putString("mode", "add");
                Navigation.findNavController(root).navigate(R.id.addGroupFragment, newBundle);
            }
        });

        return root;
    }

    private void loadGroupsFromApi(String mode) {
        switch (mode) {
            case "all":
                loadAllGroups();
                break;
            case "instructor":
                loadAllInstrucotrsGroups();
                break;
            default:
                break;
        }
    }

    private void loadAllGroups() {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getAllGroup().enqueue(new Callback<List<Group>>() {
            @Override
            public void onResponse(Call<List<Group>> call, Response<List<Group>> response) {
                if (response.isSuccessful()) {
                    groupsViewModel.setGroupsList(response.body());
                    groupsViewModel.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<Group>> call, Throwable t) {
                Log.e("tag", "error API");
            }
        });
    }

    private void loadAllInstrucotrsGroups() {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getInstructorsGroups("Bearer " + sessionManager.getUserToken(), sessionManager.getUserId()).enqueue(new Callback<List<Group>>() {
            @Override
            public void onResponse(Call<List<Group>> call, Response<List<Group>> response) {
                if (response.isSuccessful()) {
                    groupsViewModel.setGroupsList(response.body());
                    groupsViewModel.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<Group>> call, Throwable t) {
                Log.e("tag", "error API");
            }
        });
    }
}
