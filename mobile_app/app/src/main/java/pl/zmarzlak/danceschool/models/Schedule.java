package pl.zmarzlak.danceschool.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.Objects;

public class Schedule {

    @SerializedName("id")
    @Expose
    private Long id;

    @SerializedName("group")
    @Expose
    private Group group;

    @SerializedName("date")
    @Expose
    private Date date;

    @SerializedName("start")
    @Expose
    private Date start;

    @SerializedName("end")
    @Expose
    private Date end;

    public Schedule() {
    }

    public Schedule(Group group, Date date, Date start, Date end) {
        this.group = group;
        this.date = date;
        this.start = start;
        this.end = end;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Schedule schedule = (Schedule) o;
        return id.equals(schedule.id) &&
                group.equals(schedule.group);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, group);
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "id=" + id +
                ", group=" + group +
                ", date=" + date +
                ", start=" + start +
                ", end=" + end +
                '}';
    }
}
