package pl.zmarzlak.danceschool.ui.groupsList;

import android.content.Context;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.Group;


public class GroupsListViewModel extends RecyclerView.Adapter<GroupsListViewModel.ViewHolder> {

    private List<Group> groupsList;
    private View view;

    public void setGroupsList(List<Group> groupsList) {
        this.groupsList = groupsList;
    }

    public GroupsListViewModel(View view, List<Group> groupsList) {
        this.groupsList = groupsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View groupListElem = inflater.inflate(R.layout.group_list_elem, parent, false);
        ViewHolder viewHolder = new ViewHolder(groupListElem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Group group = groupsList.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putLong("groupId", group.getId());
                Navigation.findNavController(view).navigate(R.id.groupPreviewFragment, bundle);
            }
        });

        TextView name = holder.name;
        name.setText(group.getName());
    }

    @Override
    public int getItemCount() {
        return groupsList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.group_name_elem);
        }
    }

}