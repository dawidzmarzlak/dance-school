package pl.zmarzlak.danceschool.ui.register;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import pl.zmarzlak.danceschool.R;
import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.Role;
import pl.zmarzlak.danceschool.models.User;
import pl.zmarzlak.danceschool.service.APIService;
import pl.zmarzlak.danceschool.service.RetrofitClient;
import pl.zmarzlak.danceschool.service.SessionManager;
import pl.zmarzlak.danceschool.ui.editUser.EditUserViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterFragment extends Fragment {

    private RegisterViewModel registerViewModel;

    public RegisterFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_register, container, false);

        registerViewModel = ViewModelProviders.of(this).get(RegisterViewModel.class);
        final EditText username = root.findViewById(R.id.user_username_register);
        final EditText userEmail = root.findViewById(R.id.user_email_register);
        final Spinner userGroup = root.findViewById(R.id.group_spinner_register);
        final EditText userFirstname = root.findViewById(R.id.user_firstname_register);
        final EditText userLastname = root.findViewById(R.id.user_lastname_register);
        final EditText userPassword = root.findViewById(R.id.user_password_register);
        final EditText userPasswordRepeat = root.findViewById(R.id.user_password_repeat_register);

        ((Button) root.findViewById(R.id.button_register_send)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(username.getText()) || TextUtils.isEmpty(userEmail.getText())
                        || TextUtils.isEmpty(userFirstname.getText()) || TextUtils.isEmpty(userLastname.getText())
                        || !userEmail.getText().toString().contains("@") || TextUtils.isEmpty(userPassword.getText())
                        || TextUtils.isEmpty(userPasswordRepeat.getText())) {
                    Toast.makeText(getContext(), getContext().getString(R.string.fill_empty_fields), Toast.LENGTH_LONG).show();
                    userPassword.setText("");
                    userPasswordRepeat.setText("");
                } else if (!userPassword.getText().toString().equals(userPasswordRepeat.getText().toString())) {
                    Toast.makeText(getContext(), getContext().getString(R.string.bad_passwords), Toast.LENGTH_LONG).show();
                    userPassword.setText("");
                    userPasswordRepeat.setText("");
                } else {
                    User user = new User(username.getText().toString(), userEmail.getText().toString(), userPassword.getText().toString(),
                            new Role("student"), (Group) userGroup.getSelectedItem(), userFirstname.getText().toString(), userLastname.getText().toString());
                    handleRegister(root, user, userPassword, userPasswordRepeat);
                }
            }
        });

        registerViewModel.getGroups().observe(getViewLifecycleOwner(), new Observer<List<Group>>() {
            @Override
            public void onChanged(@Nullable List<Group> groups) {
                Group [] groupsArray = new Group[groups.size()];
                groups.toArray(groupsArray);
                ArrayAdapter<Group> groupArrayAdapter = new ArrayAdapter<Group>(getContext(), R.layout.support_simple_spinner_dropdown_item, groupsArray);
                userGroup.setAdapter(groupArrayAdapter);
            }
        });

        getGroups();
        return root;
    }

    private void handleRegister(View root, User user, EditText userPassword, EditText userPasswordRepeat) {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.register(user).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), getContext().getString(R.string.register_success), Toast.LENGTH_LONG).show();
                    Navigation.findNavController(root).navigate(R.id.loginFragment);
                } else {
                    Toast.makeText(getContext(),getContext().getString(R.string.edit_bad_data), Toast.LENGTH_LONG).show();
                    userPassword.setText("");
                    userPasswordRepeat.setText("");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getContext(),getContext().getString(R.string.error), Toast.LENGTH_LONG).show();
                userPassword.setText("");
                userPasswordRepeat.setText("");
            }
        });
    }

    private void getGroups() {
        APIService api = RetrofitClient.getClient().create(APIService.class);
        api.getAllGroup().enqueue(new Callback<List<Group>>() {
            @Override
            public void onResponse(Call<List<Group>> call, Response<List<Group>> response) {
                if (response.isSuccessful()) {
                    registerViewModel.setGroups(response.body());
                }

            }

            @Override
            public void onFailure(Call<List<Group>> call, Throwable t) {

            }
        });
    }
}