package pl.zmarzlak.danceschool;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class ReminderBroadcast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, context.getString(R.string.app_name))
                .setSmallIcon(R.drawable.ic_school_black_24dp)
                .setContentTitle(context.getString(R.string.upcoming_lesson))
                .setContentText(intent.getStringExtra("content"))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                .setLights(context.getColor(R.color.colorPrimary), 3000, 3000)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(context);

        managerCompat.notify(intent.getIntExtra("id", -1), builder.build());
    }
}
