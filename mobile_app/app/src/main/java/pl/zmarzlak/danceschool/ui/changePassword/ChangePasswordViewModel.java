package pl.zmarzlak.danceschool.ui.changePassword;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.LinkedList;
import java.util.List;

import pl.zmarzlak.danceschool.models.Group;
import pl.zmarzlak.danceschool.models.User;

public class ChangePasswordViewModel extends ViewModel {
    private MutableLiveData<User> mUser;

    public ChangePasswordViewModel() {
        mUser = new MutableLiveData<>();
        mUser.setValue(null);
    }

    public LiveData<User> getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser.postValue(user);
    }
}
